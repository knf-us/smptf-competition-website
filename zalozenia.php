<?php
	include("header.php");
	p_header("Podstawowe założenia");
	include("menu.php");
?>

<h1>Podstawowe założenia</h1>

<p>Konkurs jest przeznaczony dla uczniów szkół średnich. Celem konkursu
jest opracowanie szeroko rozumianego projektu multimedialnego z zakresu
fizyki (stworzeniu strony internetowej, prezentacji multimedialnej
lub programu komputerowego).</p>

<p>Konkurs ma na celu rozwinięcie wśród uczestników umiejętności:</p>
<ul>
<li>pracy indywidualnej bądź zespołowej (w zależności od rodzaju projektu),</li>
<li>posługiwania się nowoczesną techniką informatyczną i multimedialną,</li>
<li>samodzielnego rozwiązywania problemów,</li>
<li>organizowania własnej długoterminowej pracy,</li>
<li>aktywizacja środowiska szkolnego oraz lokalnego,</li>
<li>rozwijanie zainteresowań fizycznych oraz informatycznych uczestników konkursu.</li>
</ul>

<p>Wykonany projekt powinien mieć charakter popularno-naukowy (tzn. jego 
zawartość powinna być zrozumiała dla ogółu społeczeństwa) oraz charakter
popularyzatorski (tzn. powinien swoją formą i treścią zachęcać odbiorcę 
do zainteresowania się omawianą tematyką.)</p>

<p>Projekt może być wykonywany w trzech kategoriach:</p>
<ul>
<li>strona internetowa (strona o tematyce ogólnej lub poświęcona wybranemu 
zagadnieniu lub osobie znanego fizyką itp.),</li>
<li>prezentacja multimedialna (przedstawiająca w sposób ciekawy wybrane
zagadnienie, zjawisko fizyczne, badania prowadzone w światowych instytutach itp.),</li>
<li>program komputerowy (przedstawiający symulację wybranego zjawiska fizycznego itp.).</li>
</ul>

<p>Jedna szkoła może prowadzić maksymalnie dwa projekty, po jednym w dwóch 
wybranych kategoriach. Dany projekt może być wykonywany przez zespół złożony 
maksymalnie z dwóch osób.</p>

<?php
	include("footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

