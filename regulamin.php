<?php
	include("header.php");
	p_header("Regulamin");
	include("menu.php");
?>

<h1>Regulamin</h1>

<ol class="rules">
<li>Postanowienia ogólne
	<ol class="subsection">
	<li>Niniejszy regulamin określa warunki, na jakich odbywa się Ogólnopolski Konkurs na Projekt Multimedialny z Fizyki.</li>
	<li>Głównym organizatorem konkursu jest Sekcja Młodych Polskiego Towarzystwa Fizycznego z siedzibą w Katowicach; Instytut Fizyki im. Augusta Chełkowskiego, ul. Uniwersytecka 4 - pokój 167, 40-007 Katowice.</li>
	</ol>
</li>
<li>Uczestnicy konkursu
	<ol class="subsection">
	<li>Konkurs przeznaczony jest dla uczniów szkół gimnazjalnych i ponadgimnazjalnych, znajdujących się na terenie Rzeczpospolitej Polskiej.</li>
	<li>Celem konkursu jest rozwinięcie wśród uczestników umiejętności:
		<ul>
		<li>pracy indywidualnej bądź zespołowej (w zależności od rodzaju projektu),</li>
		<li>posługiwania się nowoczesną techniką informatyczną i multimedialną,</li>
		<li>samodzielnego rozwiązywania problemów,</li>
		<li>organizowania własnej długoterminowej pracy</li>
		</ul>
		oraz
		<ul>
		<li>aktywizację środowiska szkolnego oraz lokalnego,</li>
		<li>rozwijanie zainteresowań fizycznych oraz informatycznych uczestników konkursu.</li>
		</ul>
	</li>
	<li>Konkurs polega na opracowaniu projektu multimedialnego.</li>
	<li>Projekt może być wykonany w ramach jednej z trzech kategorii:
		<ul>
		<li>strona internetowa (strona o tematyce ogólnej albo poświęcona wybranemu zagadnieniu lub osobie znanego fizyka itp.);</li>
		<li>prezentacja multimedialna (przedstawiająca w sposób ciekawy wybrane zagadnienie, zjawisko fizyczne, badania prowadzone w światowych instytutach itp.);</li>
		<li>program komputerowy (przedstawiający symulacje wybranego zjawiska fizycznego itp.).</li>
		</ul>
	</li>
	<li>Jedna szkoła może prowadzić maksymalnie dwa projekty - po jednym w dwóch wybranych kategoriach. 
		<span class="emph">Szkoły będące laureatami ubiegłorocznej edycji konkursu, mogą być reprezentowane przez trzy grupy, z których maksymalnie dwie prowadzą projekt w jednej kategorii.</span></li>
	<li>Dany projekt może być wykonywany przez zespół złożony maksymalnie z dwóch uczniów.</li>
	<li>Szkoły powinny przesłać swoje zgłoszenie udziału w konkursie do końca stycznia 2006 roku, w postaci formularza zgłoszeniowego zamieszczonego na stronie internetowej konkursu. Przekroczenie tego terminu, może oznaczać niemożność uczestniczenia w konkursie.</li>
	<li>Prace konkursowe nagrane na CD-ROM powinny zostać przesłane do dnia 1 kwietnia 2006 roku, wraz z dołączoną kopertą zaadresowaną do szkoły, na adres: Sekcja Młodych Polskiego Towarzystwa Fizycznego, Instytut Fizyki im. Augusta Chełkowskiego, ul. Uniwersytecka 4 - pokój 167, 40-007 Katowice; z dopiskiem „konkurs”.</li>
	<li>Nadesłanie pracy przez szkoły równoważne jest z oświadczeniem, że autorzy materiałów nadają organizatorom prawo ich przetwarzania oraz publicznego, nieodpłatnego udostępniania w Internecie; autorzy zgadzają się ponadto na umieszczenie w/w materiałów na stronie internetowej konkursu po dokonaniu ich wstępnej kwalifikacji.</li>
	<li>Organizatorzy nie zwracają nadesłanych materiałów.</li>
	<li>Szkoły zostaną poinformowane o wynikach konkursu do końca maja 2006 roku.</li>
	<li>Finał konkursu odbędzie się 13 czerwca 2006 roku w Instytucie Fizyki Uniwersytetu Śląskiego.</li>
	<li>Uczestnicy finału zobowiązani są do krótkiego przedstawienia swojej pracy w trakcie uroczystego zakończenia konkursu.</li>
	<li>Udział w konkursie jest bezpłatny.</li>
	<li>Organizatorzy konkursu nie zobowiązują się do pokrycia kosztów przejazdu i ewentualnych kosztów zakwaterowania uczestników finału.</li>
	</ol>
	
</li>
<li>Jury oraz Sędzia Główny
	<ol class="subsection">
	<li>Nadesłane prace będą oceniane przez Jury powołane przez organizatorów konkursu.</li>
	<li>Praca Jury opiera się na pracy społecznej jego członków.</li>
	<li>Nad pracą Jury czuwa Przewodniczący wyłoniony z Jury oraz Sędzia Główny.</li>
	<li>Sędzia Główny nie jest członkiem Jury.</li>
	<li>Sędzia Główny ma decydujący głos w sprawach spornych, na których temat nie wypowiada się niniejszy regulamin, lub w sytuacjach niejasno określonych przez regulamin.</li>
	</ol>
</li>
<li>Postanowienia końcowe
	<ol class="subsection">
	<li>Kwestie nieujęte w niniejszym regulaminie, opisane są w regulaminach szczegółowych /np. w regulaminie technicznym opisującym kryteria i wymogi techniczne odnośnie prac konkursowych/.</li>
	</ol>
</li>
</ol>



<?php
	include("footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

