<?php
	include("header.php");
	p_header("Terminy");
	include("menu.php");
?>

<h1>Terminy</h1>

<h2>Zgłoszenie uczestnictwa w konkursie</h2>

<p>Szkoły zainteresowane wzięciem udziału w konkursie, powinny przesłać 
zgłoszenie na adres SM PTF do <span class="date">15 lutego 2006</span>, 
w postaci wypełnionego formularza zgłoszeniowego. W zgłoszeniu należy 
podać dokładny adres szkoły, dane nauczyciela fizyki oraz dane uczniów 
biorących udział w konkursie, wraz z krótkim opisem wykonywanego projektu 
(kategorię, charakter, przewidywaną treść).</p>

<h2>Nadsyłanie prac</h2>

<p>Prace konkursowe nagrane na płycie CD-ROM lub DVD-ROM, należy przesłać 
do dnia <span class="date">1 kwietnia 2006</span>, wraz z dołączoną kopertą 
zaadresowaną do szkoły, na adres:<br/>
<span class="addr">
Sekcja Młodych Polskiego Towarzystwa Fizycznego<br/>
Instytut Fizyki im. Augusta Chełkowskiego<br/>
ul. Uniwersytecka 4 - pokój 167<br/>
40-007 Katowice<br/>
</span> z dopiskiem "konkurs".</p>

<h2>Rozstrzygnięcie konkursu</h2>

<p>Nadesłane prace będą oceniane przez jury, powołane przez organizatorów. 
Szkoły zostaną poinformowane o wynikach do <span class="date">31 maja 2006</span>.
Prace będą również udostępnione szerszej publiczności poprzez umieszczenie 
ich na stronie internetowej konkursu.</p>

<h2>Finał</h2>

<p>Finał konkursu odbędzie się <span class="date">13 czerwca 2006</span> w 
Instytucie Fizyki Uniwersytetu Śląskiego, w sali audytoryjnej III. 
Finał będzie polegał m.in. na krótkim przedstawieniu wykonanego projektu 
jury oraz uczestnikom finału. Uczestnicy finału otrzymają atrakcyjne 
nagrody oraz pamiątkowe dyplomy.</p>

<?php
	include("footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

