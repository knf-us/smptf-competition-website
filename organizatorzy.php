<?php
	include("header.php");
	p_header("Organizatorzy");
	include("menu.php");
?>

<h1>Organizator</h1>

<p class="name">Sekcja Młodych Polskiego Towarzystwa Fizycznego</p>

<p>Adres:
<span class="addr">Uniwersytet Śląski<br />
Instytut Fizyki im. Augusta Chełkowskiego<br />
ul. Uniwersytecka 4 - pokój 167<br />
40-007 Katowice<br /><br />

<a href="http://smptf.phys.us.edu.pl/">http://smptf.phys.us.edu.pl/</a></span>
</p>

<h2>Komitet Organizacyjny</h2>

<ul>
<li><a href="mailto:aptok@us.edu.pl">Andrzej Ptok</a> - koordynator</li>
<li><a href="mailto:kbartus@us.edu.pl">Katarzyna Bartuś</a></li>
<li><a href="mailto:ajf@ich.us.edu.pl">Artur Jan Fijałkowski</a></li>
<li><a href="mailto:agrzanka@us.edu.pl">Agnieszka Grzanka</a></li>
<li><a href="mailto:mjanusz@us.edu.pl">Michał Januszewski</a></li>
</ul>

<?php
	include("footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

