<?php
	if (preg_match("/MSIE/", $_SERVER["HTTP_USER_AGENT"])) {
		$misie = true;
	} else {
		$misie = false;
	}

	if ($misie) {
		echo '<table cellpadding="0" cellspacing="0" border="0">';
		echo '<tr><td width="20%" class="menu" style="vertical-align:top;">';
	} else {
		echo <<<HTML
<div class="columns">
	<div class="wrapper">
HTML;
	}
?>
		<div class="menu" <?php 
	if ($misie) { 
		echo ' style="width:100%"'; 
	}
	echo <<<HTML

><h1>Bieżąca edycja</h1>
<ul>
<li><a href="$root_uri/index.php">Główna</a></li>
<li><a href="$root_uri/jury.php">Jury</a></li>
<li><a href="$root_uri/organizatorzy.php">Organizatorzy</a></li>
<li><a href="$root_uri/zalozenia.php">Podstawowe założenia</a></li>
<li><a href="$root_uri/regulamin.php">Regulamin</a></li>
<li><a href="$root_uri/terminy.php">Terminy</a></li>
<li><a href="$root_uri/dokumenty.php">Dokumenty do pobrania</a></li>
<li><a href="$root_uri/2006/zgloszone.php">Zgłoszone prace</a></li>
<li><a href="$root_uri/2006/nadeslane.php">Nadesłane prace</a></li>
<li><a href="$root_uri/2006/finalowe.php">Prace finałowe</a></li>
<li><a href="$root_uri/2006/final.php" style="font-weight:bold; color:yellow">Finał kokursu</a></li>
</ul>
<br/>
<h1>Poprzednia edycja</h1>
<ul>
<li><a href="$root_uri/2005/zakwalifikowane.php">Nadesłane prace</a></li>
<li><a href="$root_uri/2005/prace.php">Prace finałowe</a></li>
<li><a href="$root_uri/2005/pofinale.php">Podsumowanie</a></li>
<li><a href="http://knf.us.edu.pl/gallery/main.php?g2_view=core.ShowItem&amp;g2_itemId=5801">Zdjęcia z finału</a></li>
</ul>
<br />
		</div>
HTML;
		
	if ($misie) {
		echo '</td><td style="vertical-align:top;">';
	}
?>
		<div id="main">
		<div id="support">
<p class="first">Patronat nad konkursem objął <b>JM Rektor UŚ prof. dr hab. Janusz Janeczek.</b></p>

<p>Konkurs współfinansowany ze środków <a href="http://www.mein.gov.pl/">Ministerstwa Edukacji i Nauki</a>.</p>

<p>Sponsorzy konkursu w edycji 2005-2006:</p>
<ul>
<li><a href="http://ptf.fuw.edu.pl/">Polskie Towarzystwo Fizyczne</a></li>
<li><a href="http://www.us.edu.pl/">Uniwersytet Śląski w Katowicach</a></li>
<li><a href="http://wnt.pl/">Wydawnictwa Naukowo-Techniczne</a></li>
<li><a href="http://www.euroart.com.pl/">Euroart</a></li>
</ul>
			</div>
<?php
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

