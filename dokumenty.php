<?php
	include("header.php");
	p_header("Dokumenty do pobrania");
	include("menu.php");
?>

<h1>Dokumenty do pobrania</h1>

<table class="tbl-docs">
	<tr>
		<th>Informacje</th>
		<td><a href="dok/inf_konk.pdf">pdf</a></td>
		<td><a href="dok/inf_konk.doc">doc</a></td>
	</tr>
	<tr>
		<th>Regulamin</td>
		<td><a href="dok/reg_konk.pdf">pdf</a></td>
		<td><a href="dok/reg_konk.doc">doc</a></td>
	</tr>
	<tr>
		<th>Formularz zgłoszeniowy</th>
		<td><a href="dok/formularz.pdf">pdf</a></td>
		<td><a href="dok/formularz.doc">doc</a></td>
	</tr>
		<th>Wymogi techniczne</th>
		<td><a href="dok/tech.pdf">pdf</a></td>
		<td><a href="dok/tech.rtf">rtf</a></td>
	</tr>
	<tr>
		<th>Informacje dla Jury</th>
		<td><a href="dok/ocena_jury.pdf">pdf</a></td>
		<td><a href="dok/ocena_jury.rtf">rtf</a></td>
	</tr>
</table>

<?php
	include("footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

