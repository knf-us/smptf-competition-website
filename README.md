# smptf-competition-website
Strona Ogólnopolskiego Konkursu na Projekt Multimedialny z Fizyki 2005-2006

## Deployment

Przed deploymentem należy utworzyć plik `settings.php` np.:

```php
<?php $root_uri = "http://www.knf.us.edu.pl/smptf"; ?>
```

oraz umieścić w katalogach `2005/prace` i `2006/prace` nadesłane prace konkursowe.

## Autorzy

Autorem strony jest Michał Januszewski.
