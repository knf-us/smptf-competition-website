<?php
	include("header.php");
	p_header("Jury");
	include("menu.php");
?>

<h1>Jury</h1>

<ul>
<li>Andrzej Ptok (IF UŚ) - <span class="emph">Sędzia Główny</span></li>
</ul>

<h2>Jury Główne - edycja 2005-2006</h2>
<ol>
<li>prof. dr hab. Marek Zrałek (IF UŚ) - <span class="emph">Przewodniczący Jury</span></li>
<li>prof. dr hab. Władysław Borgieł (IF UŚ)</li>
<li>prof. dr hab. Alicja Ratuszna (IF UŚ)</li>
<li>prof. dr hab. Jerzy Warczewski (IF UŚ)</li>
<li>dr Jerzy Jarosz (IF UŚ)</li>
<li>mgr Marcin Kurpas (IF UŚ)</li>
<li>mgr Sebastian Szwarc (IF UAM)</li>
<li>mgr Szymon Rogoziński (IF UŚ)</li>
</ol>


<h2>Jury Podstawowe - edycja 2005-2006</h2>
<ol>
<li>prof. dr hab. Władysław Borgieł (IF UŚ) - <span class="emph">Przewodniczący Jury</span></li>
<li>mgr Marcin Kurpas (IF UŚ)</li>
<li>Katarzyna Bartuś (IF UŚ)</li>
<li>Artur Jan Fijałkowski (IF UŚ)</li>
<li>Agnieszka Grzanka (IF UŚ)</li>
<li>Michał Januszewski (IF UŚ)</li>
<li>Paweł Jurczyk (IF UŚ)</li>
</ol>

<h2>Jury Główne - edycja 2004-2005</h2>

<ol>
<li>prof. dr hab. Marek Zrałek (IF UŚ) - <span class="emph">Przewodniczący Jury</span></li>
<li>prof. dr hab. Jerzy Łuczka (IF UŚ)</li>
<li>prof. dr hab. Maciej Kolwas (IF PAN)</li>
<li>prof. dr hab. Wojciech Nawrocik (IF UAM)</li>
<li>prof. dr hab. Jan Gaj (IF UW)</li>
<li>prof. dr hab. Bernard Jancewicz (IFT UWr)</li>
<li>prof. dr hab. Władysław Borgieł (IF UŚ)</li>
<li>dr Jerzy Jarosz (IF UŚ)</li>
<li>mgr Alicja Joniec (IF UŚ)</li>
</ol>

<h2>Jury Podstawowe - edycja 2004-2005</h2>
<ol>
<li>dr Jerzy Jarosz (IF UŚ) - <span class="emph">Przewodniczący Jury Podstawowego</span></li> 
<li>prof. dr hab. Andrzej Burian (IF UŚ)</li>
<li>prof. dr hab. Andrzej Ślebarski (IF UŚ)</li>
<li>Katarzyna Bartuś (IF UŚ)</li>
<li>Michał Gunia (IF UŚ)</li>
<li>Agnieszka Pleban (IF UŚ)</li>

</ol>

<?php
	include("footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

