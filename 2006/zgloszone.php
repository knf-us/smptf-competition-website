<?php
	include("../header.php");
	p_header("Zgłoszone prace");
	include("../menu.php");
?>

<h1>Zgłoszone prace</h1>

<p>Lista prac, które zostały zgłoszone do konkursu:</p>
<ul>
<li><a href="docs/zgloszone.doc">wersja DOC</a></li>
<li><a href="docs/zgloszone.pdf">wersja PDF</a></li>
</ul>

<br /><br /><br /><br /><br /><br /><br /><br /><br />

<table class="entry-list">
<tr>
<th>L.p.</th>
<th>Szkoła</th>
<th>Miasto</th>
<th>Opiekun</th>
<th>Członkowie grupy</th>
<th>Kategoria</th>
</tr>
<tr>
<td>1</td>
<td>ZS im. M. Kopernika<br />Ul. Bychawska 4<br />24-200 Bełzyce</td>
<td>Bełzyce</td>
<td>Anna Słotwińska-Jakubowska</td>
<td>Mateusz Pieróg (II b)<br />Zuzanna Brewsczak (II b)</td>
<td>Strona WWW</td>
</tr><tr>
<td>2</td>
<td>ZS im. M. Kopernika<br />Ul. Bychawska 4<br />24-200 Bełzyce</td>
<td>Bełzyce</td>
<td>Anna Słotwińska-Jakubowska</td>
<td>Maciej Nagnajewicz (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>3</td>
<td>Publiczne Gimnazjum nr 5<br />Ul. Leszczynowa 16<br />21-500 Biała Podlaska</td>
<td>Biała Podlaska</td>
<td>Urszula Walczyna</td>
<td>Michał Trochiniuk (III G)<br />Damian Dożynko (III G)</td>
<td>Strona WWW</td>
</tr><tr>
<td>4</td>
<td>Publiczne Gimnazjum nr 5<br />Ul. Leszczynowa 16<br />21-500 Biała Podlaska</td>
<td>Biała Podlaska</td>
<td>Dariusz Sokolnicki</td>
<td>Bartłomiej Płoszaj (III G)<br />Michał Byśko (III G)</td>
<td>Prezentacja</td>
</tr><tr>
<td>5</td>
<td>Zespół Szkół Zawodowych nr 2<br />Ul. Brzeska<br />21-500 Biała Podlaska</td>
<td>Biała Podlaska</td>
<td>Jacek Sawczuk </td>
<td>Piotr Kamieński (III LP)</td>
<td>Strona WWW</td>
</tr><tr>
<td>6</td>
<td>I LO im. M. Kopernika<br />Ul. Listopadowa 1<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Krupitof Cuber (II)<br />Michał Kwaterki (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>7</td>
<td>I LO im. M. Kopernika<br />Ul. Listopadowa 1<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Andrzej Hanzlik (I)<br />Adrian Bydziak (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>8</td>
<td>IV LO im. KEN<br />Ul. Słowackiego 15-17<br />43-300 Bielsko Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Piotr Chalecki (I)<br />Jarosław Zahajkiewicz (I)</td>
<td>Strona WWW</td>
</tr><tr>
<td>9</td>
<td>IV LO im. KEN<br />Ul. Słowackiego 15-17<br />43-300 Bielsko Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Mateusz Ostapkowicz (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>10</td>
<td>V LO<br />Ul. Słowackiego 45<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Justyna Żarna (I)</td>
<td>Strona WWW</td>
</tr><tr>
<td>11</td>
<td>V LO<br />Ul. Słowackiego 45<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Łukasz Raszyk (I)</td>
<td>Program</td>
</tr><tr>
<td>12</td>
<td>ZS Budowlanych i Ogólnokształcących<br />Ul. Cegielniana 24<br />23-400 Biłgoraj</td>
<td>Biłgoraj</td>
<td>Elżbieta Domienik</td>
<td>Kamik Kozak (I b)</td>
<td>Prezentacja</td>
</tr><tr>
<td>13</td>
<td>I LO<br />Ul. Pl. Ks. Czaplińskiego 1<br />32-700 Bochnia</td>
<td>Bochnia</td>
<td>Elżbieta Kielar</td>
<td>Jakub Urban (III h)<br />Nanek Sebastian (III h)</td>
<td>Strona WWW</td>
</tr><tr>
<td>14</td>
<td>I LO<br />Ul. Pl. Ks. Czaplińskiego 1<br />32-700 Bochnia</td>
<td>Bochnia</td>
<td>Łukasz Cybura</td>
<td>Paweł Leśniak (III h)<br />Paweł Pędrak (III h)</td>
<td>Strona WWW</td>
</tr><tr>
<td>15</td>
<td>Publiczne Gimnazjum w Brzyskach<br />38-212 Brzyska</td>
<td>Brzyska</td>
<td>Edyta Żyguła</td>
<td>Rafał Wojdyła (G III B)</td>
<td>Program</td>
</tr><tr>
<td>16</td>
<td>Publiczne Gimnazjum w Brzyskach<br />38-212 Brzyska</td>
<td>Brzyska</td>
<td>Edyta Żyguła</td>
<td>Rafał Przewoźnik (G III B)</td>
<td>Prezentacja</td>
</tr><tr>
<td>17</td>
<td>I LO<br />Al. Mickiewicza 13<br />28-100 Busko-Zdrój</td>
<td>Busko-Zdrój</td>
<td>Mariusz Chodór</td>
<td>Konrad Turczyński (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>18</td>
<td>I LO<br />Al. Mickiewicza 13<br />28-100 Busko-Zdrój</td>
<td>Busko-Zdrój</td>
<td>Mariusz Chodór</td>
<td>Mateusz Borycki (II)</td>
<td>Program</td>
</tr><tr>
<td>19</td>
<td>I LO<br />Al. Mickiewicza 13<br />28-100 Busko-Zdrój</td>
<td>Busko-Zdrój</td>
<td>Mariusz Chodór</td>
<td>Jakub Kir (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>20</td>
<td>Samorządowe Gimnazum w Zbludowicach<br />Ul. Świętokrzyska 9<br />Zbludowice<br />28-100 Busko-Zdrój</td>
<td>Busk-Zdrój</td>
<td>Elżbieta Nowak</td>
<td>Anna Kędziak (G III)</td>
<td>Prezentacja</td>
</tr><tr>
<td>21</td>
<td>Gimnazjum nr 1 w Bychowie<br />Ul. Szkolna 8<br />23-100 Bychawa</td>
<td>Bychawa</td>
<td>Bogusław Golewski</td>
<td>Mateusz Dajos (G III)<br />Łukasz Maciąg (G III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>22</td>
<td>ZSZ nr 1<br />Ul. Reymonta 4b<br />23-100 Bychawa</td>
<td>Bychawa</td>
<td>Mgr Wiesław Olech</td>
<td>Michał Ciołek (I T Inf)</td>
<td>Strona WWW</td>
</tr><tr>
<td>23</td>
<td>ZSO w Chęłmie<br />Ul. Synów Pułku 15<br />22-100 Chełm</td>
<td>Chełm</td>
<td>Anna Jędrzejewska</td>
<td>Konrad Gajewski (III)<br />Przemysław Płonkowski (III)</td>
<td>Prezentacja</td>
</tr><tr>
<td>24</td>
<td>ZSO w Chęłmie<br />Ul. Synów Pułku 15<br />22-100 Chełm</td>
<td>Chełm</td>
<td>Anna Jędrzejewska</td>
<td>Katarzyna Saja (III)<br />Ewelina Kazanowska (III)</td>
<td>Prezentacja</td>
</tr><tr>
<td>25</td>
<td>Społeczne LO Doliny Strugu w Chmielniku<br />36-016 Chmielnik 4B</td>
<td>Chmielnik</td>
<td>Robert Chłanda</td>
<td>Łukasz Szytuła (II LO)</td>
<td>Prezentacja</td>
</tr><tr>
<td>26</td>
<td>Zespół Szkół Ponadgimnazjalnych<br />Ul. Chodzieska 29<br />64-700 Czarnków</td>
<td>Czarnków</td>
<td>Mgr Edyta Grzebyta</td>
<td>Sebastian Ratajczak (II LPb)<br />Krystyna Banachowicz (II LPb)</td>
<td>Prezentacja</td>
</tr><tr>
<td>27</td>
<td>Zespół Szkół Ponadgimnazjalnych<br />Ul. Chodzieska 29<br />64-700 Czarnków</td>
<td>Czarnków</td>
<td>Mgr Edyta Grzebyta</td>
<td>Rafał Rogosz (I ID)<br />Patryk Wożniak (I ID)</td>
<td>Strona WWW</td>
</tr><tr>
<td>28</td>
<td>I LO<br />Ul. Kopernika 40<br />41-300 Dąbrowa Górnicza</td>
<td>Dąbrowa Górnicza</td>
<td>Mgr Jadwiga Bochenek-Markiewicz</td>
<td>Joanna Subik (III)<br />Bożena Wnuk (III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>29</td>
<td>ZSZ nr 1 w Dęblinie<br />Ul. Tysiąclecia 3<br />08-530 Dęblin</td>
<td>Dęblin</td>
<td>Irena Żurkowska</td>
<td>Nowak Kamil (III LPa)<br />Plizak Łukasz (III LPa)</td>
<td>Prezentacja</td>
</tr><tr>
<td>30</td>
<td>ZSZ nr 1 w Dęblinie<br />Ul. Tysiąclecia 3<br />08-530 Dęblin</td>
<td>Dęblin</td>
<td>Irena Żurkowska</td>
<td>Domański Michał (I LPa)<br />Przybysz Bartłomiej (II LPa)</td>
<td>Program</td>
</tr><tr>
<td>31</td>
<td>Gimnazujm nr 20<br />Ul. Jowisza 60<br />81-601 Gdynia</td>
<td>Gdynia</td>
<td>Elżbieta Bartoszewska</td>
<td>Paulina Lipska (G III b)</td>
<td>Strona WWW</td>
</tr><tr>
<td>32</td>
<td>III LO<br />Ul. Gierymskiego 1<br />44-100 Gliwice</td>
<td>Gliwice</td>
<td>Katarzyna Rabijasz</td>
<td>Ilona Witkowska (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>33</td>
<td>III LO<br />Ul. Gierymskiego 1<br />44-100 Gliwice</td>
<td>Gliwice</td>
<td>Katarzyna Rabijasz</td>
<td>Agnieszka Kowalczyk (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>34</td>
<td>Zespół Szkół Łączności<br />Ul. Warszawska 35<br />Gliwice 44-100</td>
<td>Gliwice</td>
<td>Gapinska Ewa</td>
<td>Łukasz Fedorczyk (II T)<br />Paweł Król (II T)</td>
<td>Strona WWW</td>
</tr><tr>
<td>35</td>
<td>ZSP nr 11, V LO<br />Ul. Górnych Wałów<br />44-100 Gliwice</td>
<td>Gliwice</td>
<td>Mgr Beata Zimnicka</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>36</td>
<td>ZSP nr 11, V LO<br />Ul. Górnych Wałów<br />44-100 Gliwice</td>
<td>Gliwice</td>
<td>Mgr Beata Zimnicka</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>37</td>
<td>ZS im. A. Świętochowskiego<br />Ul. Ciechanowska 18b<br />06-430 Gołotczyzna</td>
<td>Gołotczyzna</td>
<td>Beata Kozłowska</td>
<td>Paweł Roman (II LP)<br />Maciej Rzeczkowski (II LP)</td>
<td>Prezentacja</td>
</tr><tr>
<td>38</td>
<td>Gimnazum nr 3<br />Ul. Szkolna 4<br />66-400 Gorzów Wielkopolski</td>
<td>Gorzów Wielkopolski</td>
<td>Krzysztof Czarnecki</td>
<td>Pęczkowski Daniel (G I)<br />Pużniak Pawel (G I)</td>
<td>Program</td>
</tr><tr>
<td>39</td>
<td>Zespół Szkół nr 2<br />Ul. 3-go Maja 1<br />22-500 Hrubieszów</td>
<td>Hrubieszów</td>
<td>Andrzej Bielak</td>
<td>Andrzej Bogatko (III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>40</td>
<td>Zespół Szkół nr 2<br />Ul. 3-go Maja 1<br />22-500 Hrubieszów</td>
<td>Hrubieszów</td>
<td>Andrzej Bielak</td>
<td>Piotr Sala<br />Rafał Petruczynik</td>
<td>Program</td>
</tr><tr>
<td>41</td>
<td>Gimnazjum im. Adama Mickiewicza<br />Ul. Główna 42<br />44-290 Jejkowice</td>
<td>Jejkowice</td>
<td>Irena Krypczyk<br />Barbara Kondrot</td>
<td>Hanna Zabka (G II)<br />Joanna Przybyła (G II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>42</td>
<td>Gimnazjum im. Adama Mickiewicza<br />Ul. Główna 42<br />44-290 Jejkowice</td>
<td>Jejkowice</td>
<td>Irena Krypczyk<br />Barbara Kondrot</td>
<td>Przemysław Giera (G III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>43</td>
<td>I LO w Kaliszu<br />Ul. Grodzka 1<br />68-800 Kalisz</td>
<td>Kalisz </td>
<td>Mgr Jadwiga Lewandowicz</td>
<td>Malwina Zurawska (I)<br />Ewelina Zurawska (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>44</td>
<td>Zespół Szkół Ponadgimnazjalnych nr 1<br />III LO w Kaliczu<br />Ul. Kościuszki 10<br />62-800 Kalisz</td>
<td>Kalisz</td>
<td>Mgr Maria Nowakowska</td>
<td>Michał Szkudlarek (I e)<br />Mateusz Radziszewski (I e)</td>
<td>Strona WWW</td>
</tr><tr>
<td>45</td>
<td>Zespół Szkół Ponadgimnazjalnych nr 1<br />III LO w Kaliczu<br />Ul. Kościuszki 10<br />62-800 Kalisz</td>
<td>Kalisz</td>
<td>Mgr Maria Nowakowska</td>
<td>Mateusz Kliber (I e)<br />Piotr Werbliński (I e)</td>
<td>Strona WWW</td>
</tr><tr>
<td>46</td>
<td>Zespół Szkół<br />Ul. M. Konopnickiej 6<br />37-220 Kańczuga</td>
<td>Kańczuga</td>
<td>Danuta Józefczyk</td>
<td>Bawor Jacek (II LP)<br />Skórksa Katarzyna (II LO)</td>
<td>Prezentacja</td>
</tr><tr>
<td>47</td>
<td>Zespół Szkół<br />Ul. M. Konopnickiej 6<br />37-220 Kańczuga</td>
<td>Kańczuga</td>
<td>Danuta Józefczyk</td>
<td>Matliński Łukasz (III LO)<br />Mazur Łukasz (III LO)</td>
<td>Strona WWW</td>
</tr><tr>
<td>48</td>
<td>Zespół Szkół<br />Ul. M. Konopnickiej 6<br />37-220 Kańczuga</td>
<td>Kańczuga</td>
<td>Danuta Józefczyk</td>
<td>Jacek Bawor (II LP)<br />Skórska Katarzyna (II LO)</td>
<td>Prezentacja</td>
</tr><tr>
<td>49</td>
<td>Zespół Szkół<br />Ul. M. Konopnickiej 6<br />37-220 Kańczuga</td>
<td>Kańczuga</td>
<td>Danuta Józefczyk</td>
<td>Łukasz Matliński (III LO)<br />Łukasz Mazur (III LO)</td>
<td>Strona WWW</td>
</tr><tr>
<td>50</td>
<td>Gimnazjum nr 21 <br />Ul. Zielonogórska 23<br />40-710 Katowice</td>
<td>Katowice</td>
<td>Ewa Chroba</td>
<td>Wojciech Snopkowski (III)<br />Jan Zając (III)</td>
<td>Prezentacja</td>
</tr><tr>
<td>51</td>
<td>Gimnazjum nr 21 <br />Ul. Zielonogórska 23<br />40-710 Katowice</td>
<td>Katowice</td>
<td>Ewa Chroba</td>
<td>Joanna Hronowska (III)<br />Małgorzata Iwańska (III)</td>
<td>Prezentacja</td>
</tr><tr>
<td>52</td>
<td>Gimnazjum nr 21 <br />Ul. Zielonogórska 23<br />40-710 Katowice</td>
<td>Katowice</td>
<td>Ewa Chroba</td>
<td>Monika Lipowska (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>53</td>
<td>Gimnazjum nr 21 <br />Ul. Zielonogórska 23<br />40-710 Katowice</td>
<td>Katowice</td>
<td>Ewa Chroba</td>
<td>Magda Kupisińska (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>54</td>
<td>II LO<br />Ul. Głowackiego 6<br />40-052 Katowice</td>
<td>Katowice</td>
<td>Adrianna Jabłońska</td>
<td>Adam Rydlewski (I e)<br />Bartosz Piekaruś (I e)</td>
<td>Prezentacja</td>
</tr><tr>
<td>55</td>
<td>II LO<br />Ul. Głowackiego 6<br />40-052 Katowice</td>
<td>Katowice</td>
<td>Adrianna Jabłońska</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>56</td>
<td>III LO<br />Ul. Mickiewicza 11<br />40-092 Katowice</td>
<td>Katowice</td>
<td>Adrianna Jabłońska</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>57</td>
<td>III LO<br />Ul. Mickiewicza 11<br />40-092 Katowice</td>
<td>Katowice</td>
<td>Adrianna Jabłońska</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>58</td>
<td>IV LO<br />Ul. Katowicka 54<br />40-165 Katowice</td>
<td>Katowice</td>
<td>Krystyna Pilot</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>59</td>
<td>IV LO<br />Ul. Katowicka 54<br />40-165 Katowice</td>
<td>Katowice</td>
<td>Krystyna Pilot</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>60</td>
<td>LO im. JFK<br />Ul. Studencka 18<br />40-743 Katowice</td>
<td>Katowice</td>
<td>Ewa Chrobak</td>
<td>Agnieszka Bukowiecka (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>61</td>
<td>LO im. JFK<br />Ul. Studencka 18<br />40-743 Katowice</td>
<td>Katowice</td>
<td>Ewa Chrobak</td>
<td>Bartosz Szurlej (II)<br />Krzysztof Szulrej (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>62</td>
<td>Prywatne Liceum Ogólnokształcące<br />Ul. Witosa 18<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Marta Skawer</td>
<td>Kamil Kawecki (II)<br />Przemysław Paliwoda (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>63</td>
<td>Prywatne Liceum Ogólnokształcące<br />Ul. Witosa 18<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Marta Skawer</td>
<td>Karol Kuczok (I)</td>
<td>Program</td>
</tr><tr>
<td>64</td>
<td>Prywatne Liceum Ogólnokształcące<br />Ul. Witosa 18<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Marta Skawer</td>
<td>Maciej Wachowiak (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>65</td>
<td>Prywatne LO<br />Ul. Witosa 18<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Marta Skwarek</td>
<td>Kamil Kawecki (II)<br />Przemysław Paliwoda (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>66</td>
<td>Prywatne LO<br />Ul. Witosa 18<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Marta Skwarek</td>
<td>Karol Kuczok (I)</td>
<td>Program komp.</td>
</tr><tr>
<td>67</td>
<td>Prywatne LO<br />Ul. Witosa 18<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Marta Skwarek</td>
<td>Maciej Wachowiak (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>68</td>
<td>Śląskie Techniczne Zakłady Naukowe<br />Ul. Sokolska 26<br />40-086 Katowice</td>
<td>Katowice</td>
<td>Marzena Wochnik</td>
<td>Izabela Fik (III c)<br />Justyna Furmaga (III c)</td>
<td>Strona WWW</td>
</tr><tr>
<td>69</td>
<td>Śląskie Techniczne Zakłady Naukowe<br />Ul. Sokolska 26<br />40-086 Katowice</td>
<td>Katowice</td>
<td>Marzena Wochnik</td>
<td>Karolina Grabek (II f)<br />Krystyna Maciak (II f)</td>
<td>Strona WWW</td>
</tr><tr>
<td>70</td>
<td>VI LO<br />Ul. Lwowska 2<br />40-389 Katowice</td>
<td>Katowice</td>
<td>Leszek Jabłoński</td>
<td>Łukasz Herb (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>71</td>
<td>VIII LO<br />Ul. 3-go Maja 42<br />40-097 Katowice</td>
<td>Katowice</td>
<td>Mgr Anna Kafel<br />Mgr Bogusław Lanuszny</td>
<td>Grzegorz Primus (III)<br />Krzysztof Łoboda (III)</td>
<td>Prezentacja</td>
</tr><tr>
<td>72</td>
<td>VIII LO<br />Ul. 3-go Maja 42<br />40-097 Katowice</td>
<td>Katowice</td>
<td>Mgr Anna Kafel<br />Mgr Bogusław Lanuszny</td>
<td>Patrycja Milewicz (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>73</td>
<td>VIII LO<br />Ul. 3-go Maja 42<br />40-097 Katowice</td>
<td>Katowice</td>
<td>Mgr Aleksandra Szydło<br />Mgr Bogusław Lanuszny</td>
<td>Piotr Omastka<br />Mateusz Wicherek</td>
<td>Strona WWW</td>
</tr><tr>
<td>74</td>
<td>ZS Ogólnokształcących nr 7<br />Ul. Witosa 23<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Dariusz Kupski</td>
<td>Marcin Breguła (I)<br />Ignacy Puśledzki (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>75</td>
<td>ZS Ogólnokształcących nr 7<br />Ul. Witosa 23<br />40-832 Katowice</td>
<td>Katowice</td>
<td>Dariusz Kupski</td>
<td>Wacław Adamczyk (III)<br />Michał Marcinkiewicz (III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>76</td>
<td>II LO<br />Ul. Śniadeckich 9<br />25-035 Kielce</td>
<td>Kielce</td>
<td>Jolanta Gajdek</td>
<td>Mateusz Kotulski (II)<br />Michał Piotrowski (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>77</td>
<td>VII LO<br />Aleja legionów 4<br />25-035 Kielce</td>
<td>Kielce</td>
<td>Beta Zięba</td>
<td>Michał Sajdak (II c)<br />Zbigniew Skuza (I d)</td>
<td>Prezentacja</td>
</tr><tr>
<td>78</td>
<td>VII LO<br />Aleja legionów 4<br />25-035 Kielce</td>
<td>Kielce</td>
<td>Beta Zięba</td>
<td>Wojciech Brojewski (I d)<br />Kajetan Koczotowski (I d)</td>
<td>Strona WWW</td>
</tr><tr>
<td>79</td>
<td>ZS nr 2V LO<br />Ul. Jedności 9<br />75-401 Koszalin</td>
<td>Koszalin</td>
<td>Małgorzata Romanowska</td>
<td>Paweł Dudek (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>80</td>
<td>XXVII LO<br />Ul. Krowoderska 17<br />31-141 Kraków</td>
<td>Kraków</td>
<td>Ewa Zięba </td>
<td>Mateusz Pasternak (II)<br />Jan Żmuda (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>81</td>
<td>XXVII LO<br />Ul. Krowoderska 17<br />31-141 Kraków</td>
<td>Kraków</td>
<td>Ewa Zięba </td>
<td>Hugo Kowicki (I)<br />Jacek Zygmunt (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>82</td>
<td>I LO<br />Ul. Piotra Skargi 2<br />38-700 Krasno</td>
<td>Krasno</td>
<td>Andrzej Piotrowski</td>
<td>Jakub Liput (I)<br />Kamil Skołowski</td>
<td>Strona WWW</td>
</tr><tr>
<td>83</td>
<td>II LO<br />Ul. Sikorkiego 25<br />23-210 Kraśnik</td>
<td>Kraśnik</td>
<td>Ewa Dec</td>
<td>Doliński Maciej<br />Żuk Maciej</td>
<td>Program</td>
</tr><tr>
<td>84</td>
<td>II LO<br />Ul. Sikorkiego 25<br />23-210 Kraśnik</td>
<td>Kraśnik</td>
<td>Ewa Dec</td>
<td>Gałkowski Maciej<br />Brożek Tomasz</td>
<td>Program</td>
</tr><tr>
<td>85</td>
<td>Zespół Szkół nr 1 w Kraśniku<br />I LO<br />Ul. Armii Krajowej 25<br />23-200 Kraśnik</td>
<td>Kraśnik</td>
<td>Mgr Deonizy Podgajny</td>
<td>Jakub Such (II a)</td>
<td>Program</td>
</tr><tr>
<td>86</td>
<td>Miejski ZS nr 1<br />Gimnazjum nr 2<br />38-400 Krosno</td>
<td>Krosno</td>
<td>Ewa Chodyniecka-Szarecka</td>
<td>Anna Paczosa (G II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>87</td>
<td>Miejski ZS nr 1<br />Gimnazjum nr 2<br />38-400 Krosno</td>
<td>Krosno</td>
<td>Ewa Chodyniecka-Szarecka</td>
<td>Jarosław Jakubowicz (G II)</td>
<td></td>
</tr><tr>
<td>88</td>
<td>LO<br />Ul. Ogorowa 3<br />32-065 Krzeszowice</td>
<td>Krzeszowice</td>
<td>Beata Welian</td>
<td>Maciej Palczewski (II c)</td>
<td>Prezentacja</td>
</tr><tr>
<td>89</td>
<td>LO<br />Ul. Ogorowa 3<br />32-065 Krzeszowice</td>
<td>Krzeszowice</td>
<td>Beata Welian</td>
<td>Grzegorz Górecki (II c)<br />Mateusz Krawczyk (II c)</td>
<td>Strona WWW</td>
</tr><tr>
<td>90</td>
<td>I LO w Legnicy<br />Plac Klasztorny 7<br />59-220 Legnica</td>
<td>Legnica</td>
<td>Mgr Mirosława Bąkowska</td>
<td>Marcin Kot (I)</td>
<td>Strona WWW</td>
</tr><tr>
<td>91</td>
<td>Gimnazjum nr 3<br />Ul. 1-go Maja 1 <br />64-100 Leszno</td>
<td>Leszno</td>
<td>Bronisław Kaźmierczak</td>
<td>Piotr Mateusz (G III a)</td>
<td>Strona WWW</td>
</tr><tr>
<td>92</td>
<td>Gimnazjum nr 3<br />Ul. 1-go Maja 1 <br />64-100 Leszno</td>
<td>Leszno</td>
<td>Bronisław Kaźmierczak</td>
<td>Przemysław Bielecki (G III a)</td>
<td>Strona WWW</td>
</tr><tr>
<td>93</td>
<td>Zespół Szkół nr 2<br />Ul. Szopena 6<br />21-100 Lubartów</td>
<td>Lubartów</td>
<td>Beata Kućminska-Trepka</td>
<td>Emil Wlazły (I b)<br />Łukasz Czerniachowski (I b)</td>
<td>Strona WWW</td>
</tr><tr>
<td>94</td>
<td>Zespół Szkół nr 2<br />Ul. Szopena 6<br />21-100 Lubartów</td>
<td>Lubartów</td>
<td>Beata Kućminska-Trepka</td>
<td>Paweł Madejski (II a)<br />Radosław Wadowski (II a)</td>
<td>Strona WWW</td>
</tr><tr>
<td>95</td>
<td>Gimnazum nr 10<br />Ul. Wajdeloty 12<br />20-604 Lublin</td>
<td>Lublin</td>
<td>Jerzy Rewiński</td>
<td>Michał Dąbrowski (G III a)<br />Paweł Stępniewski (G III a)</td>
<td>Strona WWW</td>
</tr><tr>
<td>96</td>
<td>Gimnazum nr 10<br />Ul. Wajdeloty 12<br />20-604 Lublin</td>
<td>Lublin</td>
<td>Radosław Suski</td>
<td>Mikołaj Bogusz (G III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>97</td>
<td>Gimnazjum w Łukowej<br />23-412 Łukowa</td>
<td>Łukowa</td>
<td>Agata Dąbrowska</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>98</td>
<td>Gimnazjum w Łukowej<br />23-412 Łukowa</td>
<td>Łukowa</td>
<td>Agata Dąbrowska</td>
<td>*</td>
<td>*</td>
</tr><tr>
<td>99</td>
<td>Publiczne Gimnazjum nr 2<br />Ul. Cieszkowizna 13<br />21-400 Łuków</td>
<td>Łuków</td>
<td>Katarzyna Szczęśniak</td>
<td>Natalia Łukaszewska (G III c)<br />Monika Goławska (G III c)</td>
<td>Strona WWW</td>
</tr><tr>
<td>100</td>
<td>Publiczne Gimnazjum nr 2<br />Ul. Cieszkowizna 13<br />21-400 Łuków</td>
<td>Łuków</td>
<td>Katarzyna Szczęśniak</td>
<td>Emil Wagner (G III c)<br />Rafał Karwowski (G III c)</td>
<td>Strona WWW</td>
</tr><tr>
<td>101</td>
<td>II LO w Mielcu</td>
<td>Mielec</td>
<td>Alfred Ortyl</td>
<td>Wojciech Jamrozy (I)<br />Filip Dębowski (I)</td>
<td>Program komp.</td>
</tr><tr>
<td>102</td>
<td>ZS Ekonomicznych<br />Ul. Warszawska 1<br />39-300 Mielec</td>
<td>Mielec </td>
<td>Grazyna Lasota</td>
<td>Beata Adamczyk (III LP)<br />Izabela Jemiolo (III LP)</td>
<td>Strona WWW</td>
</tr><tr>
<td>103</td>
<td>ZS Ekonomicznych<br />Ul. Warszawska 1<br />39-300 Mielec</td>
<td>Mielec </td>
<td>Grazyna Lasota</td>
<td>Kasjan Mieć (I T)<br />Damian Pleban (I T)</td>
<td>Prezentacja</td>
</tr><tr>
<td>104</td>
<td>ZST<br />Ul. K. Jagiellończyka 3<br />39-300 Mielec</td>
<td>Mielec</td>
<td>Elżbieta Dubiel-Czekaj</td>
<td>Dawid Muniak (II LP)<br />Sławonir Muniak (II LP)</td>
<td>Strona WWW</td>
</tr><tr>
<td>105</td>
<td>ZST<br />Ul. K. Jagiellończyka 3<br />39-300 Mielec</td>
<td>Mielec</td>
<td>Anna Dudek</td>
<td>Agnieszka Kaczor (I LP)<br />Betata Zych (I LP)</td>
<td>Prezentacja</td>
</tr><tr>
<td>106</td>
<td>Zespół Szkół<br />Ul. Dworcowa 5<br />34-360 Milówka</td>
<td>Milówka</td>
<td>Mgr Grzegorz Tomala</td>
<td>Kamil Rąb (I T)</td>
<td>Prezentacja</td>
</tr><tr>
<td>107</td>
<td>Gimnazum nr 4<br />34-733 Mszana Górna</td>
<td>Mszana Górna</td>
<td>Lucyna Płoskonka</td>
<td>Dariusz Filipiak (G III b)</td>
<td>Prezentacja</td>
</tr><tr>
<td>108</td>
<td>ZS nr 2 Gimnazjum nr 2 w Nisku<br />Ul. Tysiąclecia 12<br />37-400 Nisko</td>
<td>Nisko</td>
<td>Małgorzata Samek</td>
<td>Iwona Piskorowska (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>109</td>
<td>ZS nr 2 Gimnazjum nr 2 w Nisku<br />Ul. Tysiąclecia 12<br />37-400 Nisko</td>
<td>Nisko</td>
<td>Małgorzata Samek<br />Krzysztof Wcisło</td>
<td>Maciej Piotrowski (II)<br />Jarosław Żuraw (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>110</td>
<td>Gimnazjum w Opatowie<br />Ul. Szkolna 5<br />42-152 Opatów</td>
<td>Opatów</td>
<td>Lidia Pawelak<br />Teresa Kot</td>
<td>Agnieszka Mazik (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>111</td>
<td>Gimnazjum w Opatowie<br />Ul. Szkolna 5<br />42-152 Opatów</td>
<td>Opatów</td>
<td>Lidia Pawelak<br />Teresa Kot</td>
<td>Oskar Szczepaniak (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>112</td>
<td>Publiczne Gimnazjum w Osieku<br />Ul. Wolności 24a<br />28-221 Osiek</td>
<td>Osiek</td>
<td>Mgr Bożena Skoczewska</td>
<td>Martyna Równiak (G II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>113</td>
<td>Gimnazum w Ożarowicach<br />Ul. Szkolna 10<br />42-625 Ożarowice</td>
<td>Ożarowcie</td>
<td>Joanna Trefon</td>
<td>Anna Woś</td>
<td>Prezentacja</td>
</tr><tr>
<td>114</td>
<td>Gimnazum w Ożarowicach<br />Ul. Szkolna 10<br />42-625 Ożarowice</td>
<td>Ożarowcie</td>
<td>Joanna Trefon</td>
<td>Tomasz Banaś (G III)<br />Grzegorz Duda (G III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>115</td>
<td>III LO<br />Al. Armii Krajowej 17<br />97-300 Piotrków Trybunalski</td>
<td>Piotrków Trybunalski</td>
<td>Mgr Piotr Majchrzak</td>
<td>Łukasz Sutuła (III)</td>
<td>Strona WWW</td>
</tr><tr>
<td>116</td>
<td>Publiczne Gimnazum nr 1 w Piszczacu<br />Ul. Spółdzielcza 15<br />21-530 Piszczac</td>
<td>Piszczac</td>
<td>Mgr Teresa Andrzejuk<br />Mgr inż. Elżbieta Marciniuk</td>
<td>Paweł Zbański (III b)</td>
<td>Strona WWW</td>
</tr><tr>
<td>117</td>
<td>XVI LO<br />Ul. Tarnowska 27<br />61-323 Poznań</td>
<td>Poznań</td>
<td>Mgr Monika Siwiak</td>
<td>Dominik Cygalski (I)<br />Łukasz Łyszczarz (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>118</td>
<td>XVI LO<br />Ul. Tarnowska 27<br />61-323 Poznań</td>
<td>Poznań</td>
<td>Mgr Monika Siwiak</td>
<td>Paweł Tarkowski (I)<br />Michał Wojtysiak (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>119</td>
<td>ZS Ogólnokształcących nr 10<br />VIII LO i Gimnazum Dwujęzyczne<br />Ul. Zeromskiego 8/12<br />60-544 Poznań</td>
<td>Poznań</td>
<td>Danuta Hewelt</td>
<td>Aleksandra Saporznikow<br />Piotr Zaremba</td>
<td>Strona WWW</td>
</tr><tr>
<td>120</td>
<td>ZS Ogólnokształcących nr 10<br />VIII LO i Gimnazum Dwujęzyczne<br />Ul. Zeromskiego 8/12<br />60-544 Poznań</td>
<td>Poznań</td>
<td>Danuta Hewelt</td>
<td>Grzegorz Rogozik<br />Jan Kozerski</td>
<td>Prezentacja</td>
</tr><tr>
<td>121</td>
<td>Zespół Szkół Elektryczno-Elektronicznych<br />Ul. Narutowicza 12<br />97-500 Radomsko</td>
<td>Radomsko</td>
<td>Krzysztof Łęska</td>
<td>Tomasz Tymiński</td>
<td>Strona WWW</td>
</tr><tr>
<td>122</td>
<td>Gimnazujm nr 3<br />Ul. Chryzantem 10<br />41-700 Ruda Śląska </td>
<td>Ruda Śląska</td>
<td>Aleksandra Poborska</td>
<td>Adam Bismor (G I)<br />Patryk Smoląg (G I)</td>
<td>Strona WWW</td>
</tr><tr>
<td>123</td>
<td>Gimnazujm nr 8<br />Ul. Ks. P. Lexa 14<br />41-706 Ruda Śląska</td>
<td>Ruda Śląska</td>
<td>Olga Wieloch</td>
<td>Michalina Bator (G III)</td>
<td>*</td>
</tr><tr>
<td>124</td>
<td>I LO<br />Ul. Kościuszki 41<br />44-200 Rybnik</td>
<td>Rybnik</td>
<td>Jolanta Rymut</td>
<td>Marcin Święty (II f)<br />Barbara Rymuy (II f)</td>
<td>Strona WWW</td>
</tr><tr>
<td>125</td>
<td>I LO<br />Ul. Kościuszki 41<br />44-200 Rybnik</td>
<td>Rybnik</td>
<td>Jolanta Rymut</td>
<td>Rafał Kurzawa (II b)<br />Wojciech Borysewicz (II b)</td>
<td>Prezentacja</td>
</tr><tr>
<td>126</td>
<td>ZS nr 3 <br />Ul. Stróżowska 16<br />38-500 Sanok</td>
<td>Sanok</td>
<td>Arkadiusz Zalewski</td>
<td>Mariusz Munia (III Ta)<br />Krystian Kot (III Ta)</td>
<td>Prezentacja</td>
</tr><tr>
<td>127</td>
<td>Zespół Szkół<br />33-181 Siemiechów 419</td>
<td>Siemiechów</td>
<td>Mgr Rafał Radżko</td>
<td>Przemysław Gotfryd</td>
<td>Strona WWW</td>
</tr><tr>
<td>128</td>
<td>ZS Licealnych<br />Ul Bohaterów Września 3<br />69-100 Słubice</td>
<td>Słubice</td>
<td>Jan Bil<br />Maria Jaworska</td>
<td>Łukasz Gęborowski (II LO)<br />Michał Jaworski (II LO)</td>
<td>Program</td>
</tr><tr>
<td>129</td>
<td>ZS Licealnych<br />Ul Bohaterów Września 3<br />69-100 Słubice</td>
<td>Słubice</td>
<td>Jan Bil<br />Piotr Napierała</td>
<td>Jacek Krawczyk (II LO)<br />Michał Kulikowski (II LO)</td>
<td>Strona WWW</td>
</tr><tr>
<td>130</td>
<td>II LO<br />Ul. Parkowa 1<br />41-200 Sosnowiec</td>
<td>Sosnowiec</td>
<td>Mgr Wojciech Fajfer</td>
<td>Dominika Dąbrowska (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>131</td>
<td>ZS nr 3<br />Ul. Polna 15<br />37-464 Stalowa Wola 6</td>
<td>Stalowa Wola</td>
<td>Mgr Zbigniew Stopiński</td>
<td>Dawid Sobiło (III T)<br />Łukasz Sobiło (III T)</td>
<td>Strona WWW</td>
</tr><tr>
<td>132</td>
<td>ZS nr 3<br />Ul. Polna 15<br />37-464 Stalowa Wola 6</td>
<td>Stalowa Wola</td>
<td>Mgr Zbigniew Stopiński</td>
<td>Marcin Serwatka (III T)</td>
<td>Prezentacja</td>
</tr><tr>
<td>133</td>
<td>ZS Ogólnokształcących<br />LO w Suchej Beskidziekj<br />Ul. Płk. T. Semika 1<br />34-200 Sucha Beskidzka</td>
<td>Sucha Beskidzka</td>
<td>Mgr Monika Szwarc</td>
<td>Kamil Starzak (II a)<br />Szymon Pawlica (II a)</td>
<td>Program</td>
</tr><tr>
<td>134</td>
<td>ZS Ogólnokształcących<br />LO w Suchej Beskidziekj<br />Ul. Płk. T. Semika 1<br />34-200 Sucha Beskidzka</td>
<td>Sucha Beskidzka</td>
<td>Mgr Monika Szwarc</td>
<td>Szymon Biskup (II a)<br />Tomasz Elżbieciak (II a)</td>
<td>Strona WWW</td>
</tr><tr>
<td>135</td>
<td>VI LO<br />Ul. Jagiellońska 41<br />70-382 szczecin</td>
<td>Szczecin</td>
<td>Dr Aneta Mika</td>
<td>Grzegorz Bugajski (II)<br />Grzegorz Hilgier (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>136</td>
<td>VI LO<br />Ul. Jagiellońska 41<br />70-382 szczecin</td>
<td>Szczecin</td>
<td>Dr Aneta Mika</td>
<td>Agata Kowalak (III)<br />Mateusz Kownacki (III)</td>
<td>Prezentacja</td>
</tr><tr>
<td>137</td>
<td>ZS Ogólnokształcących Mistrzostwa Sportowego w Szczecinie<br />Ul. Mazurska 40<br />70-424 Szczecin</td>
<td>Szczecin</td>
<td>Mgr Maria kowalak</td>
<td>Norbert Maculewicz (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>138</td>
<td>ZS Ogólnokształcących Mistrzostwa Sportowego w Szczecinie<br />Ul. Mazurska 40<br />70-424 Szczecin</td>
<td>Szczecin</td>
<td>Mgr Maria kowalak</td>
<td>Jarosław Tysa (II)<br />Paweł Burski (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>139</td>
<td>ZS Rolnicze Centrum Kształcenia Ustawicznego<br />Ul. Henryków 54<br />67-300 Szprotawa</td>
<td>Szprotawa</td>
<td>Adrian Żak<br />Ewa Chmaj</td>
<td>Monika Drżał (II LP)<br />Arkadiusz Bała (II LP)</td>
<td>Prezentacja</td>
</tr><tr>
<td>140</td>
<td>ZS Rolnicze Centrum Kształcenia Ustawicznego<br />Ul. Henryków 54<br />67-300 Szprotawa</td>
<td>Szprotawa</td>
<td>Adrian Żak<br />Ewa Chmaj</td>
<td>Stefan Tryler (II T)</td>
<td>Prezentacja</td>
</tr><tr>
<td>141</td>
<td>Salezjański ZS Publicznych „Don Bosko”<br />Ul. Wojska Polskiego 82<br />41-709 Świętochlowice</td>
<td>Świętochłowice</td>
<td>Małgorzata Kachel</td>
<td>Sandra Jurek (I)</td>
<td>Strona WWw</td>
</tr><tr>
<td>142</td>
<td>LO z Oddziałem Integracyjnym<br />Ul. Niedziałkowskiego 2<br />72-600 Świnoujście</td>
<td>Świnoujście</td>
<td>Ewa Pater</td>
<td>Krzysztof Murawski (II)<br />Piotr Walkowiak (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>143</td>
<td>LO z Oddziałem Integracyjnym<br />Ul. Niedziałkowskiego 2<br />72-600 Świnoujście</td>
<td>Świnoujście</td>
<td>Ewa Pater</td>
<td>Dawid Niemiec (II)<br />Tomasz Piotrowski (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>144</td>
<td>ZS Techniczno-Weterynaryjnych w Trzcianie k. Rzeszowa<br />36-071 Trzciana 192</td>
<td>Trzciana</td>
<td>Barbara Selwa</td>
<td>Dariusz Pragłowski (III T)</td>
<td>Prezentacja</td>
</tr><tr>
<td>145</td>
<td>ZS Techniczno-Weterynaryjnych w Trzcianie k. Rzeszowa<br />36-071 Trzciana 192</td>
<td>Trzciana</td>
<td>Barbara Selwa</td>
<td>Piotr Apanaswicz (I T)</td>
<td>Strona WWW</td>
</tr><tr>
<td>146</td>
<td>Publiczne Gimnazjum nr 1<br />Ul. Warszawska 19<br />21-580 Wisznice</td>
<td>Wisznice</td>
<td>Małgorzata Daniłoś</td>
<td>Mateusz Wiśniewski (II c)</td>
<td>Prezentacja</td>
</tr><tr>
<td>147</td>
<td>ZS Zawodowych nr 1<br />Ul. Modrzewskiego 24<br />22-200 Włodawa</td>
<td>Włodawa</td>
<td>Mariusz Masłowski</td>
<td>Edyta Baj (II LP)</td>
<td>Prezentacja</td>
</tr><tr>
<td>148</td>
<td>I LO<br />Ul. Wiśniowa 12<br />29-100 Włoszczowa</td>
<td>Włoszczowa</td>
<td>Iwona Karaś</td>
<td>Krzysztof Barczyński (II)<br />Radosław Karaś (I)</td>
<td>Prezentacja</td>
</tr><tr>
<td>149</td>
<td>ZS Ponagimnazjalnych nr 3<br />Ul. Wiśniowa 23<br />29-100 Włoszczowa</td>
<td>Włoszczowa</td>
<td>Mgr Grażyna Bała</td>
<td>Hubert Lipniak (II LP)</td>
<td>Strona WWW</td>
</tr><tr>
<td>150</td>
<td>ZS w Wolbromiu<br />Gimnazjum nr 2<br />Ul. Pod Lasem 1<br />32-340 Wolbrom</td>
<td>Wolbrom</td>
<td>Mgr Irena Smoter</td>
<td>Justyna Gromek (IIIb)</td>
<td>Prezentacja</td>
</tr><tr>
<td>151</td>
<td>ZS w Wolbromiu<br />Gimnazjum nr 2<br />Ul. Pod Lasem 1<br />32-340 Wolbrom</td>
<td>Wolbrom</td>
<td>Mgr Irena Smoter</td>
<td>Paulina Mizura (III a)<br />Karolina Wachowicz (III a)</td>
<td>Strona WWW</td>
</tr><tr>
<td>152</td>
<td>Gimnazjum nr 18<br />Ul. Kłodnicka 56<br />54-207 Wrocław</td>
<td>Wrocław</td>
<td>Mirosław Sęga</td>
<td>Karolina Krzyszych (G II)<br />Jackiewicz Agata (G II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>153</td>
<td>Gimnazjum nr 18<br />Ul. Kłodnicka 56<br />54-207 Wrocław</td>
<td>Wrocław</td>
<td>Mirosław Sęga</td>
<td>Alberski Dominik (G II)<br />Koc Grzegorz (G II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>154</td>
<td>ZS nr 3<br />Ul. Szkocka 64<br />54-402 Wrocław</td>
<td>Wrocław</td>
<td>Dorota Sęga</td>
<td>Mirosława Olko (G II)<br />Flam Tomaz (G II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>155</td>
<td>ZS nr 3<br />Ul. Szkocka 64<br />54-402 Wrocław</td>
<td>Wrocław</td>
<td>Dorota Sęga</td>
<td>Socha Przemysław (G II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>156</td>
<td>I Społeczne LO<br />Ul. Koszatu 14<br />22-400 Zamość</td>
<td>Zamość</td>
<td>Małgorzata Bachyńska</td>
<td>Wojciech Tetrycz (I)</td>
<td>Program</td>
</tr><tr>
<td>157</td>
<td>I Społeczne LO<br />Ul. Koszatu 14<br />22-400 Zamość</td>
<td>Zamość</td>
<td>Małgorzata Bachyńska</td>
<td>Dariusz Luchowski</td>
<td>Prezentacja</td>
</tr><tr>
<td>158</td>
<td>ZS nr 2<br />Ul. Boryńska 2<br />44-240 Żory</td>
<td>Żory</td>
<td>Alicja Maryńczak</td>
<td>Łukasz Rzymanek</td>
<td>Prezentacja</td>
</tr><tr>
<td>159</td>
<td>ZS Społecznych<br />Ul. Fabryczna 10<br />44-240 Żory</td>
<td>Żory</td>
<td>Mgr Ryszard Jarkiwiecz</td>
<td>Małgorzata Szyndler (II)<br />Jarosław Dutka (I)</td>
<td>Strona WWW</td>
</tr><tr>
<td>160</td>
<td>ZS Społecznych<br />Ul. Fabryczna 10<br />44-240 Żory</td>
<td>Żory</td>
<td>Mgr Ryszard Jarkiwiecz</td>
<td>Jonasz Wala (II)<br />Łukasz Bączek (I)</td>
<td>Program</td>
</tr><tr>
<td>161</td>
<td>Gimnazjum nr 2<br />Ul. Zielona 1<br />34-300 Żywiec</td>
<td>Żywiec</td>
<td>Teresa Smulska</td>
<td>Przemysław Obrocki (III)<br />Robert Występek (II)</td>
<td>Strona WWW</td>
</tr><tr>
<td>162</td>
<td>Gimnazjum nr 2<br />Ul. Zielona 1<br />34-300 Żywiec</td>
<td>Żywiec</td>
<td>Teresa Smulska</td>
<td>Grzegorz Kozieł (III)<br />Dawid Wieczorek (II)</td>
<td>Prezentacja</td>
</tr><tr>
<td>163</td>
<td>ZS Mechaniczno-Elektrycznych<br />Ul. KEN 3<br />34-300 Żywiec</td>
<td>Żywiec</td>
<td>Stanisław Juraszek</td>
<td>Krzysztof Piecuch (III T)<br />Marcin Juraszek (II LO)</td>
<td>Program</td>
</tr>
</table>

<?php
	include("../footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

