<?php
	include("../header.php");
	p_header("Finał konkursu");
	include("../menu.php");
?>

<h1>Finał konkursu</h1>

<p>Informujemy, że wszystkie drużyny podtwierdziły udział w finale,
wobec czego drużyny rezerwowe nie biorą czynnego udziału w finale.
Zapraszamy jednak do udziału w uroczystościach (kibicowania finalistom). 
Życzymy wielu sukcesów w przyszłości.</p>

<p><a href="docs/final_plan.pdf">Plan finału</a> do ściągnięcia (PDF).</p>

<p><a href="docs/final_kolejnosc.pdf">Kolejność wystąpień finałowych</a> (PDF).</p>

<h2>Wyniki konkursu</h2>

<p>Jury Główne ustaliło podczas obrad, że o wynikach finału decyduje jedynie 
liczba punktów zdobytych na finale.</p>
<ul>
<li><a href="docs/wyniki.pdf">PDF</a></li>
<li><a href="docs/wyniki.doc">DOC</a></li>
</ul>

<h2>Informacja na temat oceniania prac przez Jury</h2>
<ul>
<li><a href="docs/ocena_jury.pdf">PDF</a></li>
<li><a href="docs/ocena_jury.rtf">RTF</a></li>
</ul>

<p>Załącznik do Regulaminu Ogólnopolskiego Konkursu na Projekt Multimedialny z Fizyki</p>

<ol>
<li>Prace dopuszczone do oceny przeszły wstępną selekcję 
/która objęła wymagania regulaminowe oraz techniczne odnośnie prac/.</li>
<li>Prace oceniane są w II etapach:
<ol>
	<li>ocena przez Jury Główne – podczas finału</li>
	<li>ocena przez Jury Podstawowe</li>
</ol>
</li>

<li>Pracom odpowiednich jury przewodniczą:
<ul>
<li>Przewodniczący Jury Głównego: prof. dr hab. Marek Zrałek</li>
<li>Przewodniczący Jury Podstawowego: prof. dr hab. Władysław Borgieł</li>
</ul>
</li>

<li>Prace podczas finału oceniane są w skali 10-io stopniowej 
/10 – najlepiej, 0 – najgorzej/. Prosimy oceniać:
<ul>
<li>Zawartość merytoryczną pracy</li>
<li>Odczucia estetyczne</li>
<li>Wykorzystaną technikę</li>
<li>Intuicyjność oraz łatwość obsługi</li>
</ul>
</li> 

<li>Jurorów prosi się o samodzielną ocenę prac oraz niekonsultowanie oceny z 
innymi członkami Jury.</li>

<li>Podczas finału Jury Główne ocenia prace wg wyżej zamieszczonych kryteriów 
oraz na podstawie wystąpienia autora/ów.</li>

<li>Wystąpienia finałowe autorów odbywają się według poniższej 
kolejności działań z zachowaniem podanych limitów czasowych:
<ul>
<li>wystąpienie autorów pracy: 10 minut</li>
<li>pytania Jury oraz Sędziego Głównego: 3 minuty</li>
</ul>
Uczestnicy finału oraz zaproszeni goście nie mogą zadawać pytań (brak pytań „z sali”).
</li>

<li>O zwycięstwie decyduje suma punktów podczas finału oraz oceny wstępne 
(przyznane przez Jury Podstawowe i zamieszczone na stronie internetowej 
konkursu). Podczas remisu decydujący wpływ ma ocena Przewodniczącego Jury. 
W przypadku dalszego remisu decydujący głos ma Sędzia Główny.</li>
</ol>


<h2>Plan dojazdu</h2>
<br />
<img src="plan_dojazdu.jpg" alt="Plan dojazdu" />

<br /><br />


<h2>Lista prac finałowych</h2>

<br />

<table class="entry-list">
<tr>
<th>ID</th>
<th>Szkoła</th>
<th>Miasto</th>
<th>Opiekun</th>
<th>Członkowie grupy</th>
<th>Liczba punktów</th>
</tr>
<tr>
	<th colspan="7">Strony WWW</th>
</tr>
<tr>
<td><a href="prace/036/">036</a></td>
<td>ZSP nr 11, V LO<br />Ul. Górnych Wałów<br />44-100 Gliwice</td>
<td>Gliwice</td>
<td>Mgr Beata Zimnicka</td>
<td>Anna Spyrzyńska<br />Piotr Wittchen</td>
<td>79,33</td>
</tr>
<tr>
<td><a href="prace/006/">006</a></td>
<td>I LO im. M. Kopernika<br />Ul. Listopadowa 1<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Krupitof Cuber (II)<br />Michał Kwaterki (II)</td>
<td>77,67</td>
</tr>
<tr>
<td><a href="prace/129/">129</a></td>
<td>ZS Licealnych<br />Ul Bohaterów Warszawy 3<br />69-100 Słubice</td>
<td>Słubice</td>
<td>Jan Bil<br />Piotr Napierała</td>
<td>Jacek Krawczyk (II LO)<br />Michał Kulikowski (II LO)</td>
<td>67,00</td>
</tr>
<tr>
<td><a href="prace/135/">135</a></td>
<td>VI LO<br />Ul. Jagiellońska 41<br />70-382 szczecin</td>
<td>Szczecin</td>
<td>Dr Aneta Mika</td>
<td>Grzegorz Bugajski (II)<br />Grzegorz Hilgier (II)</td>
<td>65,79</td>
</tr>
<tr>
<td><a href="prace/073/">073</a></td>
<td>VIII LO<br />Ul. 3-go Maja 42<br />40-097 Katowice</td>
<td>Katowice</td>
<td>Mgr Aleksandra Szydło<br />Mgr Bogusław Lanuszny</td>
<td>Piotr Omastka<br />Mateusz Wicherek</td>
<td>65,00</td>
</tr>
<tr>
	<th colspan="7">Program komputerowy</th>
</tr>
<tr>
<td><a href="prace/128/">128</a></td>
<td>ZS Licealnych<br />Ul Bohaterów Warszawy 3<br />69-100 Słubice</td>
<td>Słubice</td>
<td>Jan Bil<br />Maria Jaworska</td>
<td>Łukasz Gęborowski (II LO)<br />Michał Jaworski (II LO)</td>
<td>69,60</td>
</tr>
<tr>
<td><a href="prace/011/">011</a></td>
<td>V LO<br />Ul. Słowackiego 45<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Łukasz Raszyk (I)</td>
<td>57,88</td>
</tr>
<tr>
<td><a href="prace/084/">084</a></td>
<td>Zespół Szkół nr 2<br />II LO<br />Ul. Sikorkiego 25<br />23-210 Kraśnik</td>
<td>Kraśnik</td>
<td>Ewa Dec</td>
<td>Gałkowski Maciej<br />Brożek Tomasz</td>
<td>51,48</td>
</tr>
<tr>
<td><a href="prace/085/">085</a></td>
<td>Zespół Szkół nr 1 w Kraśniku<br />I LO<br />Ul. Armii Krajowej 25<br />23-200 Kraśnik</td>
<td>Kraśnik</td>
<td>Mgr Deonizy Podgajny</td>
<td>Jakub Such (II a)</td>
<td>49,13</td>
</tr>
<tr>
<td><a href="prace/163/">163</a></td>
<td>ZS Mechaniczno-Elektrycznych<br />Ul. KEN 3<br />34-300 Żywiec</td>
<td>Żywiec</td>
<td>Stanisław Juraszek</td>
<td>Krzysztof Piecuch (III T)<br />Marcin Juraszek (II LO)</td>
<td>48,00</td>
</tr>
<tr>
	<th colspan="7">Prezentacje multimedialne</th>
</tr>
<tr>
<td><a href="prace/050/">050</a></td>
<td>Gimnazjum nr 21 <br />Ul. Zielonogórska 23<br />40-710 Katowice</td>
<td>Katowice</td>
<td>Ewa Chrobak</td>
<td>Wojciech Snopkowski (III)<br />Jan Zając (III)</td>
<td>84,57</td>
</tr>
<tr>
<td><a href="prace/126/">126</a></td>
<td>ZS nr 3 <br />Ul. Stróżowska 16<br />38-500 Sanok</td>
<td>Sanok</td>
<td>Arkadiusz Zulewski</td>
<td>Mariusz Munia (III Ta)<br />Krystian Kot (III Ta)</td>
<td>76,29</td>
</tr>
<tr>
<td><a href="prace/117/">117</a></td>
<td>XVI LO<br />Ul. Tarnowska 27<br />61-323 Poznań</td>
<td>Poznań</td>
<td>Mgr Monika Siwiak</td>
<td>Dominik Cygalski (I)<br />Łukasz Łyszczarz (I)</td>
<td>68,20</td>
</tr>
<tr>
<td><a href="prace/019/">019</a></td>
<td>I LO<br />Al. Mickiewicza 13<br />28-100 Busko-Zdrój</td>
<td>Busko-Zdrój</td>
<td>Mariusz Chodór</td>
<td>Jakub Kir (I)</td>
<td>60,00</td>
</tr>
<tr>
<td><a href="prace/055/">055</a></td>
<td>II LO<br />Ul. Głowackiego 6<br />40-052 Katowice</td>
<td>Katowice</td>
<td>Leszek Jabłoński</td>
<td>Łukasz Herb (II)</td>
<td>59,74</td>
</tr>
</table>

<?php
	include("../footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

