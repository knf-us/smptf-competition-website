<?php
	include("../header.php");
	p_header("Prace finałowe");
	include("../menu.php");
?>

<h1>Prace finałowe</h1>

<p>Serdecznie gratulujemy wszystkim finalistom.</p>

<p>Dziękujemy wszystkim drużynom za udział w konkursie i życzymy wielu sukcesów w przyszłości.</p>

<p>Finalistów prosimy o zapoznanie się z <a href="docs/inform_f.pdf">informacjami fla finalistów</a>.</p>

<p>Lista prac finałowych do ściągnięcia:</p>
<ul>
<li><a href="docs/finalowe.doc">wersja DOC</a></li>
<li><a href="docs/finalowe.pdf">wersja PDF</a></li>
</ul>

<br /><br />

<table class="entry-list">
<tr>
<th>ID</th>
<th>Szkoła</th>
<th>Miasto</th>
<th>Opiekun</th>
<th>Członkowie grupy</th>
<th>Liczba punktów</th>
</tr>
<tr>
	<th colspan="7">Strony WWW</th>
</tr>
<tr>
<td><a href="prace/036/">036</a></td>
<td>ZSP nr 11, V LO<br />Ul. Górnych Wałów<br />44-100 Gliwice</td>
<td>Gliwice</td>
<td>Mgr Beata Zimnicka</td>
<td>Anna Spyrzyńska<br />Piotr Wittchen</td>
<td>79,33</td>
</tr>
<tr>
<td><a href="prace/006/">006</a></td>
<td>I LO im. M. Kopernika<br />Ul. Listopadowa 1<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Krupitof Cuber (II)<br />Michał Kwaterki (II)</td>
<td>77,67</td>
</tr>
<tr>
<td><a href="prace/129/">129</a></td>
<td>ZS Licealnych<br />Ul Bohaterów Warszawy 3<br />69-100 Słubice</td>
<td>Słubice</td>
<td>Jan Bil<br />Piotr Napierała</td>
<td>Jacek Krawczyk (II LO)<br />Michał Kulikowski (II LO)</td>
<td>67,00</td>
</tr>
<tr>
<td><a href="prace/135/">135</a></td>
<td>VI LO<br />Ul. Jagiellońska 41<br />70-382 szczecin</td>
<td>Szczecin</td>
<td>Dr Aneta Mika</td>
<td>Grzegorz Bugajski (II)<br />Grzegorz Hilgier (II)</td>
<td>65,79</td>
</tr>
<tr>
<td><a href="prace/073/">073</a></td>
<td>VIII LO<br />Ul. 3-go Maja 42<br />40-097 Katowice</td>
<td>Katowice</td>
<td>Mgr Aleksandra Szydło<br />Mgr Bogusław Lanuszny</td>
<td>Piotr Omastka<br />Mateusz Wicherek</td>
<td>65,00</td>
</tr>
<tr class="backup">
<td><a href="prace/080/">080</a> <span style="color:red; font-weight:bold">R</span></td>
<td>XXVII LO<br />Ul. Krowoderska 17<br />31-141 Kraków</td>
<td>Kraków</td>
<td>Ewa Zięba </td>
<td>Mateusz Pasternak (II)<br />Jan Żmuda (II)</td>
<td>63,14</td>
</tr>
<tr class="backup">
<td><a href="prace/082/">082</a> <span style="color:red; font-weight:bold">R</span></td>
<td>I LO<br />Ul. Piotra Skargi 2<br />38-700 Krasno</td>
<td>Krosno</td>
<td>Andrzej Piotrowski</td>
<td>Jakub Liput (I)<br />Kamil Skołowski</td>
<td>63,00</td>
</tr>
<tr class="backup">
<td><a href="prace/017/">017</a> <span style="color:red; font-weight:bold">R</span></td>
<td>I LO<br />Al. Mickiewicza 13<br />28-100 Busko-Zdrój</td>
<td>Busko-Zdrój</td>
<td>Mariusz Chodór</td>
<td>Konrad Turczyński (II)</td>
<td>62,00</td>
</tr>
<tr>
	<th colspan="7">Program komputerowy</th>
</tr>
<tr>
<td>128</td>
<td>ZS Licealnych<br />Ul Bohaterów Warszawy 3<br />69-100 Słubice</td>
<td>Słubice</td>
<td>Jan Bil<br />Maria Jaworska</td>
<td>Łukasz Gęborowski (II LO)<br />Michał Jaworski (II LO)</td>
<td>69,60</td>
</tr>
<tr>
<td>011</td>
<td>V LO<br />Ul. Słowackiego 45<br />43-300 Bielsko-Biała</td>
<td>Bielsko-Biała</td>
<td>Janina Kula</td>
<td>Łukasz Raszyk (I)</td>
<td>57,88</td>
</tr>
<tr>
<td>084</td>
<td>Zespół Szkół nr 2<br />II LO<br />Ul. Sikorkiego 25<br />23-210 Kraśnik</td>
<td>Kraśnik</td>
<td>Ewa Dec</td>
<td>Gałkowski Maciej<br />Brożek Tomasz</td>
<td>51,48</td>
</tr>
<tr>
<td>085</td>
<td>Zespół Szkół nr 1 w Kraśniku<br />I LO<br />Ul. Armii Krajowej 25<br />23-200 Kraśnik</td>
<td>Kraśnik</td>
<td>Mgr Deonizy Podgajny</td>
<td>Jakub Such (II a)</td>
<td>49,13</td>
</tr>
<tr>
<td>163</td>
<td>ZS Mechaniczno-Elektrycznych<br />Ul. KEN 3<br />34-300 Żywiec</td>
<td>Żywiec</td>
<td>Stanisław Juraszek</td>
<td>Krzysztof Piecuch (III T)<br />Marcin Juraszek (II LO)</td>
<td>48,00</td>
</tr>
<tr>
	<th colspan="7">Prezentacje multimedialne</th>
</tr>
<tr>
<td>050</td>
<td>Gimnazjum nr 21 <br />Ul. Zielonogórska 23<br />40-710 Katowice</td>
<td>Katowice</td>
<td>Ewa Chrobak</td>
<td>Wojciech Snopkowski (III)<br />Jan Zając (III)</td>
<td>84,57</td>
</tr>
<tr>
<td>126</td>
<td>ZS nr 3 <br />Ul. Stróżowska 16<br />38-500 Sanok</td>
<td>Sanok</td>
<td>Arkadiusz Zulewski</td>
<td>Mariusz Munia (III Ta)<br />Krystian Kot (III Ta)</td>
<td>76,29</td>
</tr>
<tr>
<td>117</td>
<td>XVI LO<br />Ul. Tarnowska 27<br />61-323 Poznań</td>
<td>Poznań</td>
<td>Mgr Monika Siwiak</td>
<td>Dominik Cygalski (I)<br />Łukasz Łyszczarz (I)</td>
<td>68,20</td>
</tr>
<tr>
<td>019</td>
<td>I LO<br />Al. Mickiewicza 13<br />28-100 Busko-Zdrój</td>
<td>Busko-Zdrój</td>
<td>Mariusz Chodór</td>
<td>Jakub Kir (I)</td>
<td>60,00</td>
</tr>
<tr>
<td>055</td>
<td>II LO<br />Ul. Głowackiego 6<br />40-052 Katowice</td>
<td>Katowice</td>
<td>Leszek Jabłoński</td>
<td>Łukasz Herb (II)</td>
<td>59,74</td>
</tr>
<tr class="backup">
<td>029 <span style="color:red; font-weight:bold">R</span></td>
<td>ZSZ nr 1 w Dęblinie<br />Ul. Tysiąclecia 3<br />08-530 Dęblin</td>
<td>Dęblin</td>
<td>Irena Żurkowska</td>
<td>Nowak Kamil (III LPa)<br />Plizak Łukasz (III LPa)</td>
<td>57,15</td>
</tr>
<tr class="backup">
<td>159 <span style="color:red; font-weight:bold">R</span></td>
<td>ZS Społecznych<br />Ul. Fabryczna 10<br />44-240 Żory</td>
<td>Żory</td>
<td>Mgr Ryszard Jarkiwiecz</td>
<td>Małgorzata Szyndler (II)<br />Jarosław Dutka (I)</td>
<td>57,14</td>
</tr>
</table>

<p class="backup">Ciemniejsze tło oznacza oraz literka 
<span style="color:red; font-weight:bold">R</span> przy numerze pracy oznaczają
drużynę rezerwową, która bierze udział w finale w przypadku rezygnacji z udziału 
w finale przez ktorąś z drużyn finałowych.</p>

<?php
	include("../footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

