<?php
	include("../header.php");
	p_header("Prace finałowe");
	include("../menu.php");
?>

<h1>Prace finałowe</h1>

<p>Do finału zakwalifikowało się <span class="emph">siedem</span> prezentacji 
multimedialnych, <span class="emph">pięć</span> programów komputerowych oraz 
<span class="emph">siedem</span> stron internetowych. W przypadku rezygnacji 
z udziału w finale przez drużynę, która została zakwalifikowana, prawo udziału 
przysługuje grupom rezerwowym zgodnie z kolejnością w tabeli poniżej.</p>

<p>Gratulujemy finalistom!</p>

<p>Lista prac finałowych do ściągnięcia:</p>
<ul>
<li><a href="dok/lista_final.doc">wersja DOC</a></li>
<li><a href="dok/lista_final.pdf">wersja PDF</a></li>
</ul>

<br /><br /><br />

<table class="entry-list">
<tr>
	<th>Lp.</th>
	<th>Szkoła</th>
	<th>Miasto</th>
	<th>Opiekun</th>
	<th>Członkowie grupy</th>
	<th>Tytuł</th>
	<th>Ocena</th>
</tr>
<tr>
	<th colspan="7">Prezentacje Multimedialne</th>
</tr>
<tr>
	<td>
	29.
	</td>

	<td>
		ZSO nr I im. Mikołaja Kopernika<br/>
		40-039 Katowice<br/>
		ul. Sienkiewicza 74<br/>
		t/f 0*32 2563601;2551863<br/>
		<a href="http://www.kopernik.w3.com.pl/">www.kopernik.w3.com.pl</a><br/>

		<a href="mailto:sekretarzkopernik@op.pl">sekretarzkopernik@op.pl</a>
	</td>
	<td>
	Katowice
	</td>
	<td>
	Marek Goszczyński
	</td>
	<td>

		Justyna Kosmala (II LO)
	</td>
	<td>
	Wszechświat
	</td>
	<td>
	7,17
	</td>
	</tr>
	<tr>

	<td>
	23.
	</td>
	<td>
		VIII LO w Katowicach<br/>
		Ul. 3-go Maja 42<br/>
		40-097 Katowice<br/>
		t. 2539432<br/>

	<a href="mailto:pik@womkat.edu.pl">pik@womkat.edu.pl</a>
	</td>
	<td>
	Katowice
	</td>
	<td>
		Anna Kafel<br/>
	Bogusław Lanuszny
	</td>

	<td>
		Grzegorz Primus (II LO, 18 l)<br/>
		Krzysztof Łoboda (II LO, 18 l)<br/>
	</td>
	<td>
	Wielki Wybuch
	</td>
	<td>

	7,00
	</td>
	</tr>
	<tr>
	<td>
	68.
	</td>
	<td>
		Liceum Ogólnokształcące Wyższej Szkoły Informatyki i Zarządzania<br/>

		Ul. Mjr. H. Sucharskiego 2<br/>
		35-225 Rzeszów<br/>
		<a href="mailto:lo@wsiz.rzeszow.pl">lo@wsiz.rzeszow.pl</a><br/>
	</td>
	<td>
	Rzeszów
	</td>
	<td>

	Aleksander Ciereszyński 
	</td>
	<td>
	Wojciech Leja (II LO, 18 l)
	</td>
	<td>
	Dźwięk
	</td>
	<td>
	7,00
	</td>

	</tr>
	<tr>
	<td>
	11.
	</td>
	<td>
		Zespół Szkół w Czarnej<br/>
		Publiczne Gimnazjum<br/>

		Ul. Konarskiego 23<br/>
		39-215 Czarna<br/>
		t. 0-14 6761011<br/>
	</td>
	<td>
	Czarna
	</td>
	<td>

	Mgr Marta Konieczna
	</td>
	<td>
		Marlena Jędrocka (I G, 13 l)<br/>
		Anna Orloł (I G, 13 l)<br/>
	</td>
	<td>
	Zjawisko tęczy
	</td>

	<td>
	6,83
	</td>
	</tr>
	<tr>
	<td>
	6.
	</td>
	<td>
		I LO im. T. Kościuszki<br/>

		Al. Mickiewicza 13<br/>
		28-100 Busko-Zdrój<br/>
	<a href="mailto:achod@ki.onet.pl">achod@ki.onet.pl</a>
	</td>
	<td>
	Busko-Zdrój
	</td>
	<td>

	Mariusz Chodór
	</td>
	<td>
	Konrad Turczyński (I LO)
	</td>
	<td>
	Słońce &ndash; najpiękniejsza gwiazda Świata
	</td>
	<td>

	6,67
	</td>
	</tr>
	<tr>
	<td>
	80.
	</td>
	<td>
		Zespół Szkół Nr 3 w Sanoku<br/>
		Ul. Stróżowska 16<br/>
		38-500 Sanok<br/>
	</td>
	<td>
	Sanok
	</td>
	<td>
	Mgr Arkadiusz Zulewski
	</td>

	<td>
		Idec Tomasz (II T, 17 l)<br/>
		Kot Krystian (II T, 17 l)<br/>
	</td>
	<td>
	Fale elektromagnetyczne &ndash; zastosowanie w moim miejscu zamieszkania
	</td>

	<td>
	6,60
	</td>
	</tr>
	<tr>
	<td>
	83.
	</td>
	<td>
		Zespół Szkół Licealnych im. Z. Herberta<br/>

		Ul. Bohaterów Warszawy 3<br/>
		69-100 Słubice<br/>
	<a href="mailto:loslubice@interia.pl">loslubice@interia.pl</a>
	</td>
	<td>
	Słubice
	</td>
	<td>

	Jan Bil
	</td>
	<td>
		Jaworski Michał (I , 17 l)<br/>
		Paczkowski Piotr (I , 17 l)<br/>
	</td>
	<td>
	Obwód LC
	</td>

	<td>
	6,50
	</td>
</tr>
<tr>
	<th colspan="7">&nbsp;</th>
</tr>
	<tr>
	<td>
	92.
	</td>

	<td>
		Zespół Szkół Ogólnokształcących Mistrzostwa Sportowego w Szczecinie<br/>
		Gimnazjum 19<br/>
		Ul. Mazurska 40<br/>
	
	</td>
	<td>
	Szczecin
	</td>

	<td>
	Maria Kowalak
	</td>
	<td>
		Szymon Myśliński (III G, 15 l)<br/>
		Sebastian Ortel (III G, 15 l)<br/>
	</td>
	<td>

	Fizyka i astronomia
	</td>
	<td>
	6,33
	</td>
	</tr>
	<tr>
	<td>
	24.
	</td>

	<td>
		Gimnazjum nr 21<br/>
		Ul. Zielonogórska 23<br/>
		40-710 Katowice<br/>
	<a href="mailto:gimnazjum21@gimnazjum21.katowice.pl">gimnazjum21@gimnazjum21.katowice.pl</a>
	</td>
	<td>

	Katowice
	</td>
	<td>
	Ewa Chrobak
	</td>
	<td>
	Jan Zając
	</td>
	<td>
	Powstawanie Wszechświata
	</td>

	<td>
	6,00
	</td>
	</tr>
	<tr>
	<td>
	15.
	</td>
	<td>
		Zespół Szkół im. A. Świętochowskiego<br/>

		Ul. Ciechanowska 18<br/>
		06-430 Gołotczyzna<br/>
	<a href="mailto:bratne@gazeta.pl">bratne@gazeta.pl</a>
	</td>
	<td>
	Gołotczyzna
	</td>
	<td>

	Beata Kozłowska
	</td>
	<td>
		Agata Sobieraj (II LP, 17 l)<br/>
		Kamil Niesłuchowski (II LP, 17 l)<br/>
	</td>
	<td>
	Energia w przyrodzie
	</td>

	<td>
	5,67
	</td>
	</tr>
	<tr>
	<td>
	33.
	</td>
	<td>
		Zespół Szkół w Konieczkowej<br/>

		Konieczkowa 58<br/>
		38-114 Niebylec<br/>
	</td>
	<td>
	Konieczkowa
	</td>
	<td>
		Mgr Magdalena Rozborska<br/>

		Mgr Iwona Flisiak<br/>
	</td>
	<td>
		Edyta Królikowska (III G, 15 l)<br/>
	</td>
	<td>
	Niebo nade mną (Ewolucja gwiazd)
	</td>

	<td>
	5,67
	</td>
	</tr>
<tr>
	<th colspan="7">Programy komputerowe</th>
</tr>

	<tr>
	<td>
	32.
	</td>
	<td>
		I LO im. Henryka Sienkiewicza<br/>
		95-040 Koluszki<br/>
		ul. Kościuszki 16<br/>

		t/f 0-44 7141489<br/>
	<a href="mailto:lo1koluszki@szkoly.lodz.pl">lo1koluszki@szkoly.lodz.pl</a>
	</td>
	<td>
	Koluszki
	</td>
	<td>
	Mgr Edyta Szeleszczyk
	</td>

	<td>
		Sylwester Sokołowski (III LO, 19 l)<br/>
	</td>
	<td>
	Fizyka
	</td>
	<td>
	6,83
	</td>

	</tr>
	<tr>
	<td>
	7.
	</td>
	<td>
		Zespół Szkół Ponadgimnazjalnych<br/>
		Ul. Słowackiego 4<br/>

		57-500 Bystrzyca Kłodzka<br/>
	<a href="mailto:zspbystrzycakl@o2.pl">zspbystrzycakl@o2.pl</a>
	</td>
	<td>
	Bystrzyca Kłodzka
	</td>
	<td>
		Sylwia Pierzchała-Ryznar<br/>

		Przemysław Stuglik
	</td>
	<td>
		Paweł Marcak (II T, 17 l)<br/>
		Sławomir Sijka (II T, 17 l)<br/>
	</td>
	<td>
	Projektant tubusów
	</td>

	<td>
	6,20
	</td>
	</tr>
	<tr>
	<td>
	34.
	</td>
	<td>
		II LO w Końskich<br/>

		Ul. Sportowa 9<br/>
		26-200 Końskie<br/>
		t/f 0-41 3722556
	</td>
	<td>
	Końskie
	</td>
	<td>
	Robert Misztal
	</td>

	<td>
		Michał Świtakowski (II LO, 17 l)<br/>
	e-mail: <a href="mailto:swistakers@wp.pl">swistakers@wp.pl</a>
	
	
	</td>
	<td>
	Wahadło Matematyczne 1.0
	</td>
	<td>

	5,60
	</td>
	</tr>
	<tr>
	<td>
	84.
	</td>
	<td>
		Zespół Szkół Licealnych im. Z. Herberta<br/>

		Ul. Bohaterów Warszawy 3<br/>
		69-100 Słubice<br/>
	<a href="mailto:loslubice@interia.pl">loslubice@interia.pl</a>
	</td>
	<td>
	Słubice
	</td>
	<td>

	Jan
	Bil
	</td>
	<td>
		Gembarowski Łukasz (I , 17 l)<br/>
		Andrzejewski Przemysław (I , 17 l)
	</td>
	<td>
	Układ słoneczny
	</td>
	<td>

	5,40
	</td>
	</tr>
	<tr>
	<td>
	101.
	</td>
	<td>
		XIV LO im. St. Staszica<br/>

		Ul. Nowowiejska 37a<br/>
		02-010 Warszawska<br/>
	</td>
	<td>
	Warszawa
	</td>
	<td>
	Stanisław Lipiński
	</td>

	<td>
		Krzysztof Krogulski (I LO, 16 l)<br/>
		Maciej Bulwacki (I LO, 16 l)
	</td>
	<td>
	Doświadczenie Rutherforda
	</td>
	<td>
	5,00
	</td>

	</tr>
	<tr>
		<th colspan="7">Strony WWW</th>
	</tr>
	<tr>
	<td>

	10.
	</td>
	<td>
		II LO im. K. K. Baczyńskiego<br/>
		Ul. Wyszyńskiego 19<br/>
		32-500 Chrzanów
	</td>
	<td>
	Chrzanów
	</td>

	<td>
	Andrzej	Saługa
	</td>
	<td>
		Janusz Mikrut (II LO, 17 l)<br/>
	Joanna Rejduch (II LO, 17 l)
	</td>
	<td>
	Komety
	</td>

	<td>
	7,00
	</td>
	</tr>
	<tr>
	<td>
	49.
	</td>
	<td>
		XXIX LO im. Hm. Jana Bytnara &bdquo;Rudego&ldquo;<br/>

		Ul. Zelwerowicza 38/44<br/>
		90-147 Łódź<br/>
	tel./fax (0 42) 678 94 82
	</td>
	<td>
	Łódź
	</td>
	<td>
		Zbigniew Sobór<br/>

		Grażyna	Baczyńska
	</td>
	<td>
		Kamil Szyndel (II LO, 17 l)<br/>
	Jakub Kołaczkowski (II LO, 17 l)
	</td>
	<td>
		Woda a efekt Mpemby
	</td>
	<td>

	7,00
	</td>
	</tr>
	<tr>
	<td>
	79.
	</td>
	<td>
		I LO im. Komisji Edukacji Narodowej<br/>

		Ul. Zagrody 1<br/>
		38-500 Sanok<br/>
	</td>
	<td>
	Sanok
	</td>
	<td>
	Ewa Mazepa
	</td>

	<td>
		Tomasz Wolański (II LO, 17 l)<br/>
	Tomasz Soźnicki (II LO, 17 l)
	</td>
	<td>
	Energia jadrowa i jej wykorzystanie
	</td>
	<td>
	6,83
	</td>

	</tr>
	<tr>
	<td>
	3.
	</td>
	<td>
		IV LO im. KEN<br/>
		Im. Słowackiego 15-17<br/>

	43-300 Bielsko-Biała
	</td>
	<td>
	Bielsko-Biała
	</td>
	<td>
	Janina Kula
	</td>
	<td>
		Piotr Majdał<br/>

		Michał Pocał<br/>
	</td>
	<td>
	Silniki
	</td>
	<td>
	6,80
	</td>
	</tr>

	<tr>
	<td>
	51.
	</td>
	<td>
		Gimnazjum nr 4 w Mielcu<br/>
		39-303 Mielec<br/>
		ul. Łąkowa 6<br/>

	<a href="mailto:gim4@miasto.mielec.pl">gim4@miasto.mielec.pl</a>
	</td>
	<td>
	Mielec
	</td>
	<td>
		Elżbieta Wiącek<br/>
	<a href="mailto:elawia2@wp.pl">elawia2@wp.pl</a>

	</td>
	<td>
		Magdalena Broda (III, 16 l)<br/>
		Michał Giża (III, 16 l)<br/>
	</td>
	<td>
	Fizyka wokół nas
	</td>

	<td>
	6,40
	</td>
	</tr>
	<tr>
	<td>
	85.
	</td>
	<td>
		Zespół Szkół Nr 3 im. Króla jana III Sobieskiego w	Stalowej Woli<br/>

		Ul. Polna 15<br/>
		37-464 Stalowa Wola<br/>
	t/f 0-15 8440412
	</td>
	<td>
	Stalowa Wola
	</td>
	<td>
	Mgr Zbigniew Stopiński
	</td>

	<td>
		Dawid Sobiło (II T Hand, 18 l)<br/>
	Łukasz Sobiło (II T Hand, 18 l)
	</td>
	<td>
	Księżyc
	</td>
	<td>
	6,17
	</td>

	</tr>
	<tr>
	<td>
	95.
	</td>
	<td>
		I LO im .Ks. Elżbiety w Szczecinku<br/>
		Ul. Ks. Elżbiety 1<br/>

	<a href="mailto:loela@poczta.onet.pl">loela@poczta.onet.pl</a>
	</td>
	<td>
	Szczecinek
	</td>
	<td>
	Mgr inż. Anna Baryłowicz
	</td>
	<td>

	Paweł Galerczyk
	</td>
	<td>
	Fizyka
	</td>
	<td>
	5,75
	</td>
	</tr>
<tr>
	<th colspan="7">&nbsp;</th>
</tr>
	<tr>
	<td>
	13.
	</td>
	<td>
		Zespół Szkół Ogólnokształcących nr 11<br/>
		Ul. Górnych Watów 29<br/>
		44-100 Gliwice<br/>

	<a href="mailto:strug@Vlo.gliwice.pl">strug@Vlo.gliwice.pl</a>
	</td>
	<td>
	Gliwice
	</td>
	<td>
	Mgr Beata Kaczorowska
	<a href="mailto:kaczorow@us.edu.pl">kaczorow@us.edu.pl</a>
	<BR>

	
	</td>
	<td>
		Michał Mikulski (III LO, 18 l)<br/>
		Tomasz Pluskiewicz (III LO, 18 l)
	
	</td>
	<td>
	Układ Słoneczny
	</td>
	<td>

	5,60
	</td>
</tr>
	<tr>
	<td>
	45.
	</td>
	<td>
		Zespół Szkół Ponadgimnazjalnych nr IX<br/>
		Al. Politechniki 38<br/>

		93-590 Łódź<br/>
	<a href="mailto:dankras@poczta.fm">dankras@poczta.fm</a>
	</td>
	<td>
	Łódź
	</td>
	<td>
	Danuta Krasocka
	</td>
	<td>
		Piotr Kobierzycki (III LO, 19 l)<br/>
	Paweł Orzechowski (III LO, 19 l)
	</td>
	<td>
	Fizyka w medycynie
	</td>
	<td>
	5,60
	</td>

	</tr>
	<tr>
	<td>
	75.
	</td>
	<td>
		Zespół Szkół Nr 2 w Rzeszowie<br/>
		Ul. Rejtana 3<br/>

		35-326 Rzeszów<br/>
	</td>
	<td>
	Rzeszów
	</td>
	<td>
	Maria Smolak
	</td>
	<td>

		Mirosław Rykała (II LO, 18 l)<br/>
	Grzegorz Rak (II LO, 18 l)
	</td>
	<td>
	Strona WWW o Marii Skłodowskiej-Curie
	</td>
	<td>
	5,60
	</td>
	</tr>
</table>

<?php
	include("../footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

