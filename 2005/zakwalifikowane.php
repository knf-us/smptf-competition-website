<?php
	include("../header.php");
	p_header("Prace finałowe");
	include("../menu.php");
?>

<h1>Nadesłane prace</h1>

<p>Lista prac, które zostały nadesłane na konkurs:</p>
<ul>
<li><a href="dok/konkurs_nadesl.doc">wersja DOC</a></li>
<li><a href="dok/konkurs_nadesl.pdf">wersja PDF</a></li>
</ul>

<p>Prace, które przeszły wstępną selekcję (zgodność z regulaminem konkursu i wymogami technicznymi):</p>
<ul>
<li><a href="dok/konkurs_do_oceny.doc">wersja DOC</a></li>
<li><a href="dok/konkurs_do_oceny.pdf">wersja PDF</a></li>
</ul>

<br /><br /><br />

<table class="entry-list">
	<tr>
		<th>Lp.</th>
		<th>Szkoła</th>
		<th>Członkowie grupy</th>
		<th>Kategoria</th>
		<th>Tytuł</th>
	</tr>
	<tr>
		<td>
			3.
		</td>
		<td>
			IV LO im. KEN
			Im.
			Słowackiego 15-17
			43-300
			Bielsko-Biała
		</td>
		<td>

			Piotr
			Majdał
			Michał
			Pocał
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Silniki
		</td>
	</tr>

	<tr>
		<td>
			4.
		</td>
		<td>
			V
			LO im. KEN
			Ul.
			Słowackiego 45
			43-300
			Bielsko-Biała
			t/f
			0-33 8123911; 8123336
		</td>
		<td>
			Agnieszka
			Drabik
			Marta
			Gałdyś
		</td>

		<td>
			Strona
			WWW
		</td>
		<td>
			Leopold Infeld
		</td>
	</tr>
	<tr>
		<td>

			5.
		</td>
		<td>
			V
			LO im. KEN
			Ul.
			Słowackiego 45
			43-300
			Bielsko-Biała
			t/f
			0-33 8123911; 8123336
		</td>
		<td>
			Anna
			Lach
			Joanna
			Siąkała
		</td>
		<td>
			Prezentacja
		</td>

		<td>
			Fale
			elektromagnetyczne
		</td>
	</tr>
	<tr>
		<td>
			6.
		</td>
		<td>
			I
			LO im. T. Kościuszki
			Al.
			Mickiewicza 13
			28-100
			Busko-Zdrój
			<a href="mailto:achod@ki.onet.pl">achod@ki.onet.pl</a>

		</td>
		<td>
			Konrad
			Turczyński
			(I
			LO)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Słońce
			&ndash; najpiękniejsza gwiazda świata
		</td>

	</tr>
	<tr>
		<td>
			7.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych
			Ul.
			Słowackiego 4
			57-500
			Bystrzyca Kłodzka
			<a href="mailto:zspbystrzycakl@o2.pl">zspbystrzycakl@o2.pl</a>
		</td>

		<td>
			Paweł
			Marcak
			(II
			T, 17 l)
			Sławomir
			Sijka
			(II
			T, 17 l)
		</td>
		<td>
			Program
			komp.
		</td>
		<td>
			Projektant
			tubusów
		</td>
	</tr>

	<tr>
		<td>
			8.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych
			Ul.
			Słowackiego 4
			57-500
			Bystrzyca Kłodzka
			<a href="mailto:zspbystrzycakl@o2.pl">zspbystrzycakl@o2.pl</a>
		</td>
		<td>

			Radosław
			Dzięcioł
			(II
			LO, 18 l)			Kamil
			Kasica
			(III
			LP, 19 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Strona WWW o A. Einsteinie
		</td>
	</tr>

	<tr>
		<td>
			10.
		</td>
		<td>
			II
			LO im. K. K. Baczyńskiego
			Ul.
			Wyszyńskiego 19
			32-500
			Chrzanów
		</td>
		<td>
			Janusz
			Mikrut
			(II
			LO, 17 l)
			Joanna
			Rejduch
			(II
			LO, 17 l)
		</td>

		<td>
			Strona WWW
		</td>
		<td>
			Komety
		</td>
	</tr>
	<tr>
		<td>

			11.
		</td>
		<td>
			Zespół
			Szkół w Czarnej
			Publiczne
			Gimnazjum
			Ul.
			Konarskiego 23
			39-215
			Czarna
			t.
			0-14 6761011
		</td>
		<td>
			Marlena
			Jędrocka
			(I
			G, 13 l)
			Anna
			Orloł
			(I
			G, 13 l)
		</td>
		<td>
			Prezentacja
		</td>

		<td>
			Zjawisko
			tęczy
		</td>
	</tr>
	<tr>
		<td>
			13.
		</td>
		<td>
			Zespół
			Szkół Ogólnokształcących nr 11
			Ul.
			Górnych Watów 29
		44-100
			Gliwice
			<a href="mailto:strug@Vlo.gliwice.pl">strug@Vlo.gliwice.pl</a>

		</td>
		<td>
			Michał
			Mikulski
			(III
			LO, 18 l)
			Tomasz
			Pluskiewicz
			(III
			LO, 18 l) 
			
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Układ Słoneczny

		</td>
	</tr>
	<tr>
		<td>
			14.
		</td>
		<td>
			Zespół
			Szkół Ogólnokształcących nr 11
			Ul.
			Górnych Watów 29
			44-100
			Gliwice
			<a href="mailto:strug@Vlo.gliwice.pl">strug@Vlo.gliwice.pl</a>

		</td>
		<td>
			Wojciech
			Kmita
			(III
			LO, 18 l)
			Wojciech
			Piechowiak
			(III
			LO, 18 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Komety

		</td>
	</tr>
	<tr>
		<td>
			15.
		</td>
		<td>
			Zespół
			Szkół im. A. Świętochowskiego
			Ul.
			Ciechanowska 18
			06-430
			Gołotczyzna
			<a href="mailto:bratne@gazeta.pl">bratne@gazeta.pl</a>

		</td>
		<td>
			Agata
			Sobieraj
			(II
			LP, 17 l)
			Kamil
			Niesłuchowski
			(II
			LP, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Energia
			w przyrodzie
		</td>

	</tr>
	<tr>
		<td>
			17.
		</td>
		<td>
			Zespół
			Szkół Licealnych i Technicznych im. S. Staszica
			Ul.
			Racławicka 1
			66-620
			Gubin
			<a href="http://www.zslit.gubin.pl/">www.zslit.gubin.pl</a>
		</td>

		<td>
			Kamil
			Małczyński
			(II
			LO, 17 l)
			Rene
			Olszewski
			(II
			LO, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Fizyka
			a &bdquo;elektryki&rdquo;
		</td>

	</tr>
	<tr>
		<td>
			19.
		</td>
		<td>
			II
			LO im.Ppłk. Józefa Modrzejewskiego
			38-200
			Jasło
			Ul.
			Floriańska 24
			T
			4463570; 4463488
		</td>
		<td>
			Dawid
			Kieca
			(I
			LO, 17 l);
			Szymon
			Augustyn
			(I
			LO, 17 l)		</td>

		<td>
			Strona
			WWW
		</td>
		<td>
			Spacerem po wszechświecie
		</td>
	</tr>
	<tr>
		<td>

			20.
		</td>
		<td>
			II
			LO im. Ppłk. J. Modrzejewskiego w Jaśle
			Ul.
			Floriańska 24
			38-200
			Jasło
		</td>
		<td>
			Kornelia
			Pruchnik
			(III
			LO, 18 l)
			Jolanta
			Górska
			(III
			LO, 18 l)
		</td>
		<td>
			Prezentacja
		</td>

		<td>
			Komety
			<BR>
			
		</td>
	</tr>
	<tr>
		<td>
			21.
		</td>
		<td>

			Zespół
			Szkół Ponadgimnazjalnych nr 2
			Ul.
			Inwalidów Wojennych 16
			43-600
			Jaworzno
		</td>
		<td>
			Jadwiga
			Paluch
			(II
			LP, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Galaktyki
		</td>

	</tr>
	<tr>
		<td>
			22.
		</td>
		<td>
			VIII
			LO w Katowicach
			Ul.
			3-go Maja 42
			40-097
			Katowice
			t.
			2539432
			<a href="mailto:pik@womkat.edu.pl">pik@womkat.edu.pl</a>
		</td>

		<td>
			Krzystof
			Heinrich
			(II
			LO, 17 l)
			Sebastian
			Marek
			(II
			LO, 18 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Edwin Hubbel
		</td>

	</tr>
	<tr>
		<td>
			23.
		</td>
		<td>
			VIII
			LO w Katowicach
			Ul.
			3-go Maja 42
			40-097
			Katowice
			t.
			2539432
			<a href="mailto:pik@womkat.edu.pl">pik@womkat.edu.pl</a>
		</td>

		<td>
			Grzegorz
			Primus
			(II
			LO, 18 l)
			Krzysztof
			Łoboda
			(II
			LO, 18 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Wielki
			Wybuch
		</td>
	</tr>

	<tr>
		<td>
			24.
		</td>
		<td>
			Gimnazjum
			nr 21
			Ul.
			Zielonogórska 23
			40-710
			Katowice
			<a href="mailto:gimnazjum21@gimnazjum21.katowice.pl">gimnazjum21@gimnazjum21.katowice.pl</a>
		</td>
		<td>

			Jan
			Zając
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Powstawanie
			Wszechświata
		</td>
	</tr>
	<tr>

		<td>
			27.
		</td>
		<td>
			Nowe
			Liceum
			LO
			Ul.
			Studencka 10
			40-714
			Katowice
		</td>
		<td>
			Mateusz
			Kol
		</td>
		<td>

			Prezentacja
		</td>
		<td>
			Dźwięki
		</td>
	</tr>
	<tr>
		<td>
			29.
		</td>

		<td>
			ZSO
			nr I im. Mikołaja Kopernika
			40-039
			Katowice
			ul.
			Sienkiewicza 74
		t/f
		0*32 2563601;2551863
		<a href="http://www.kopernik.w3.com.pl/">www.kopernik.w3.com.pl</a>
			<a href="mailto:sekretarzkopernik@op.pl">sekretarzkopernik@op.pl</a>
		</td>
		<td>
			Justyna
			Kosmala
			(II
			LO)
		</td>
		<td>

			Prezentacja
		</td>
		<td>
			Wszechświat
		</td>
	</tr>
	<tr>
		<td>
			31.
		</td>

		<td>
			II
			LO im. J. Śniadeckiego
			Ul.
			Śwideckich 9
			25-366
			Kielce
			<a href="mailto:sniadek@sniadek.kie.pl">sniadek@sniadek.kie.pl</a>
		</td>
		<td>
			Michał
			Piotrowski
			(I
			LO, 16 l)
		</td>
		<td>
			Strona
			WWW
		</td>

		<td>
			Johannes Keppler
		</td>
	</tr>
	<tr>
		<td>
			32.
		</td>
		<td>

			I
			LO im. Henryka Sienkiewicza
			95-040
			Koluszki
			ul.
			Kościuszki 16
			<P LANG="de-DE" CLASS="western">t/f
			0-44 7141489
			<a href="mailto:lo1koluszki@szkoly.lodz.pl">lo1koluszki@szkoly.lodz.pl</a>
		</td>
		<td>
			Sylwester
			Sokołowski
			(III
			LO, 19 l)
		</td>
		<td>
			Program
			komp.
		</td>

		<td>
			Fizyka
		</td>
	</tr>
	<tr>
		<td>
			33.
		</td>
		<td>
			Zespół
			Szkół w Konieczkowej
			Konieczkowa
			58
			38-114
			Niebylec
		</td>

		<td>
			Edyta
			Królikowska
			(III
			G, 15 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Niebo
			nade mną
			(Ewolucja
			gwiazd)
		</td>
	</tr>

	<tr>
		<td>
			34.
		</td>
		<td>
			II
			LO w Końskich
			Ul.
			Sportowa 9
			26-200
			Końskie
			t/f
			0-41 3722556
		</td>
		<td>
			Michał
			Świtakowski
			(II
			LO, 17 l)
			e-mail:
			<a href="mailto:swistakers@wp.pl">swistakers@wp.pl</a>			
		</td>

		<td>
			Program
			komp.
		</td>
		<td>
			Wahadło
			Matematyczne 1.0
		</td>
	</tr>
	<tr>
		<td>
			36.
		</td>

		<td>
			I
			LO im. Tadeusza Kościuszki
			Pl.
			Klasztorny 7
			59-220
			Legnica
		</td>
		<td>
			Przemysław
			Pabisiak
			Maciej
			Trojniarz
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>

			Ciekawa Fizyka
		</td>
	</tr>
	<tr>
		<td>
			38.
		</td>
		<td>
			Zespół
			Szkół nr 2
			Ul.
			Szopena 6
			21-100
			Lubartów
		</td>

		<td>
			Olga
			Rejek
			(I
			, 16 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Niels Bohr
		</td>

	</tr>
	<tr>
		<td>
			39.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych nr 1 w Łęczycy
			Ul.
			Ozorkowskie Przedmieście 2
			99-100
			Łęczyca
			t.
			0*24 7212341
			<a href="mailto:zsp1@oswiata.org.pl">zsp1@oswiata.org.pl</a>
		</td>

		<td>
			Adam
			Szewczyk
			(II
			LP, 18 l)
			Łukasz
			Różalski
			(II
			LP, 18 l)
		</td>
		<td>
			Strona WWW
		</td>
		<td>
			Mikołaj
			Kopernik
		</td>

	</tr>
	<tr>
		<td>
			40.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych nr 1 w Łęczycy
			Ul.
			Ozorkowskie Przedmieście 2
			99-100
			Łęczyca
			t.
			0*24 7212341
			<a href="mailto:zsp1@oswiata.org.pl">zsp1@oswiata.org.pl</a>
		</td>

		<td>
			Grzegorz
			Kondras
			(I
			T, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Burza
		</td>
	</tr>

	<tr>
		<td>
			44.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych nr IX
			Al.
			Politechniki 38
			93-590
			Łódź
			<a href="mailto:dankras@poczta.fm">dankras@poczta.fm</a>
		</td>
		<td>

			Kamil
			Kukawski
			(III
			LO, 19 l)
			Adam
			Radwański
			(III
			LO, 19 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Fizyka
			zagrożeń środowiska
		</td>
	</tr>
	<tr>

		<td>
			45.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych nr IX
			Al.
			Politechniki 38
			93-590
			Łódź
			<a href="mailto:dankras@poczta.fm">dankras@poczta.fm</a>
		</td>
		<td>
			Piotr
			Kobierzycki
			(III
			LO, 19 l)
			Paweł
			Orzechowski
			(III
			LO, 19 l)
		</td>

		<td>
			Strona
			WWW
		</td>
		<td>
			Fizyka w medycynie
		</td>
	</tr>
	<tr>
		<td>

			46.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych nr 21
			Ul.
			Żubardzka 2
			91-022
			Łódź
			t.
			6517898
			f.:
			6407280
		</td>
		<td>
			Tomasz
			Kuljan
			(III
			T, 18 l)
			Witold
			Poliński
			(III
			T, 18 l)
		</td>
		<td>
			Prezentacja
		</td>

		<td>
			Zorze
			polarne
		</td>
	</tr>
	<tr>
		<td>
			47.
		</td>
		<td>
			Zespół
			Szkół Ponadgimnazjalnych nr 21
			Ul.
			Żubardzka 2
			91-022
			Łódź
			t.
			6517898
			f.:
			6407280
		</td>

		<td>
			Mariusz
			Machnicki
			(III
			T, 18 l)
			Mariusz
			Gulczyński
			(III
			T, 19 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Wyładowania atmosferyczne
		</td>

	</tr>
	<tr>
		<td>
			48.
		</td>
		<td>
			Zespół
			Szkół Ogólnokształcących Nr 10
			LO
			XVIII im. J. Śniadeckiego
			94-203
			Łódź
			ul.
			Perla 11
			t.
			0-42 6339323
		</td>
		<td>
			Krzysztof
			Szer
			(II
			LO, 17 l)
			Krzysztof
			Hejkowski
			(II
			LO, 17 l)
		</td>

		<td>
			Strona
			WWW
		</td>
		<td>
			Projekt im. Rolanda Maze
		</td>
	</tr>
	<tr>
		<td>

			49.
		</td>
		<td>
			XXIX
			LO im. Hm. Jana Bytnara &bdquo;Rudego&ldquo;
			Ul.
			Zelwerowicza 38/44
			90-147
			Łódź
			tel./fax
			(0 42) 678 94 82
		</td>
		<td>
			Kamil
			Szyndel
			(II
			LO, 17 l)
			Jakub
			Kołaczkowski
			(II
			LO, 17 l)
		</td>

		<td>
			Strona
			WWW
		</td>
		<td>
			Woda a efekt Mpemby
			
		</td>
	</tr>
	<tr>
		<td>

			50.
		</td>
		<td>
			Gimnazjum
			nr 4 w Mielcu
			39-303
			Mielec
			ul.
			Łąkowa 6
			<a href="mailto:gim4@miasto.mielec.pl">gim4@miasto.mielec.pl</a>
		</td>
		<td>
			Krystian
			Mężyk
			(II,
			15 l)
		</td>
		<td>

			Prezentacja
		</td>
		<td>
			Prezentacja
			o Albercie Einsteinie
		</td>
	</tr>
	<tr>
		<td>
			51.
		</td>

		<td>
			Gimnazjum
			nr 4 w Mielcu
			39-303
			Mielec
			ul.
			Łąkowa 6
			<a href="mailto:gim4@miasto.mielec.pl">gim4@miasto.mielec.pl</a>
		</td>
		<td>
			Magdalena
			Broda
			(III,
			16 l);
			Michał
			Giża
			(III,
			16 l)
		</td>
		<td>
			Strona
			WWW
		</td>

		<td>
			Fizyka wokół nas
		</td>
	</tr>
	<tr>
		<td>
			53.
		</td>
		<td>

			Zespół
			Szkół w Milówce
			Ul.
			Dworcowa 5
			34-360
			Milówka
		</td>
		<td>
			Małgorzata
			Juraszek
			Andrzej
			Duraj
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Fizyka w astronomii

		</td>
	</tr>
	<tr>
		<td>
			55.
		</td>
		<td>
			Liceum
			Ogólnokształcące
			Ul.
			Szpitalna 10
			05-500
			Piaseczno
			<a href="mailto:zs1_p@wp.pl">zs1_p@wp.pl</a>

		</td>
		<td>
			Ewa
			Tomczak
			(II
			LO, 17 l)
			Emilia
			Wichowska
			(II
			LO, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Tajemnice
			Kosmosu
		</td>

	</tr>
	<tr>
		<td>
			56.
		</td>
		<td>
			Technikum
			Ul.
			Szpitalna 10
			05-500
			Piaseczno
			<a href="mailto:zs1_p@wp.pl">zs1_p@wp.pl</a>
		</td>

		<td>
			Arkadiusz
			Biernat
			(I
			T, 17 l)
			Jacek
			Ofat
			(I
			T, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Elektryzowanie
			się ciał
		</td>
	</tr>

	<tr>
		<td>
			58.
		</td>
		<td>
			Zespół
			Szkół Nr 1
			Il.
			Sieńkiweicza 8
			09-100
			Płońsk
		</td>
		<td>
			Paweł
			Giżyński
			(III
			LO, 19 l)
			Sławomir
			Karpiński
			(III
			LO, 19 l)
		</td>

		<td>
			Prezentacja
		</td>
		<td>
			Światło
			i jego zjawiska
		</td>
	</tr>
	<tr>
		<td>
			60.
		</td>

		<td>
			Zespół
			Szkół  KOMORNO
			Ul.
			Harcerska 81
			47-214
			Poborszów
		</td>
		<td>
			Marcin
			Szafarczyk
			(18
			l, III L)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>

			Albert Einstein
		</td>
	</tr>
	<tr>
		<td>
			61.
		</td>
		<td>
			VI
			LO
			Ul.
			Krakowska 17 a
			61-889
			Poznań
		</td>

		<td>
			Ken
			Rządek
			(17
			l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Akceleratory
			cząstek naładowanych i ich zastosowanie w badaniach naukowych JINR
			 Dubna
		</td>
	</tr>

	<tr>
		<td>
			62.
		</td>
		<td>
			IV
			LO im. Komisji Edukacji Narodowej
			Ul.
			Swojska 6
			60-592
			Poznań
			t.
			0-61 8417771
		</td>
		<td>
			Jakub
			Sołtysiak
			(I
			LO, 16 l)
			Jędrzej
			Mosięzny
			(I
			LO, 16l)
		</td>

		<td>
			Prezentacja
		</td>
		<td>
			Kiedy
			&bdquo;Star Trek&rdquo; stanie się rzeczywistością
		</td>
	</tr>
	<tr>

		<td>
			63.
		</td>
		<td>
			Zespół
			Szkół w Przykonie
			Gimnazjum
			w Przykonie
			Ul.
			Szkolna 4a
			62-731
			Przykonie
		</td>
		<td>
			Agnieszka
			Kulesza
			(I
			G, 14 l)
		</td>
		<td>

			Prezentacja
		</td>
		<td>
			Dynamika
			skoków narciarskich
		</td>
	</tr>
	<tr>
		<td>
			64.
		</td>

		<td>
			Zespół
			Szkół w Przykonie
			Gimnazjum
			w Przykonie
			Ul.
			Szkolna 4a
			62-731
			Przykonie
		</td>
		<td>
			Bartosz
			Jurkiewicz
			(III
			G, 16 l)
			Natalia
			Cichomska
			(III
			G, 16 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>

			Słońce
			- wielka elektrownia
		</td>
	</tr>
	<tr>
		<td>
			66.
		</td>
		<td>
			Zespół
			Szkół Nr 1 im. Jana Pawła II
			Al.
			WOP 13
			26-400
			Przysucha
			<a href="mailto:zsp@przysucha.pl">zsp@przysucha.pl</a>

		</td>
		<td>
			Izabela
			Dąbrowska
			(I
			, 16 l)
			Agnieszka
			Grudziecka
			(I,
			16 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Archimedes

		</td>
	</tr>
	<tr>
		<td>
			67.
		</td>
		<td>
			Zespół
			Szkół Mechanicznych im. Gen. Władysława Andersa w Rzeszowie
			Ul.
			Hetmańska 45a
		</td>
		<td>

			Rafał
			Klimczak
			(V
			T, 20 l)
			Piotr
			Borowicz
			(V
			T, 20 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Zjawisko
			fotoelektryczne
		</td>
	</tr>
	<tr>

		<td>
			68.
		</td>
		<td>
			Liceum
			Ogólnokształcące Wyższej Szkoły Informatyki i Zarządzania
			Ul.
			Mjr. H. Sucharskiego 2
			35-225
			Rzeszów
			<a href="mailto:lo@wsiz.rzeszow.pl">lo@wsiz.rzeszow.pl</a>
		</td>
		<td>
			Wojciech
			Leja
			(II
			LO, 18 l)
		</td>

		<td>
			Prezentacja
		</td>
		<td>
			Dźwięk
		</td>
	</tr>
	<tr>
		<td>
			69.
		</td>

		<td>
			Liceum
			Ogólnokształcące Wyższej Szkoły Informatyki i Zarządzania
			Ul.
			Mjr. H. Sucharskiego 2
			35-225
			Rzeszów
			<a href="mailto:lo@wsiz.rzeszow.pl">lo@wsiz.rzeszow.pl</a>
		</td>
		<td>
			Paweł
			Szwaracki
			(II
			LO, 17 l)
		</td>
		<td>
			Prezentacja
		</td>

		<td>
			Światło
		</td>
	</tr>
	<tr>
		<td>
			70.
		</td>
		<td>
			Gimnazjum
			nr 9 im. Św. Królowej Jadwigi
			Ul.
			E. Orzeszkowej 8a
			35-006
			Rzeszów
		</td>

		<td>
			Rafał
			Kułaga
			(III
			G, 15 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Fizyka
			Wokół Nas
		</td>
	</tr>

	<tr>
		<td>
			71.
		</td>
		<td>
			Gimnazjum
			nr 9 im. Św. Królowej Jadwigi
			Ul.
			E. Orzeszkowej 8a
			35-006
			Rzeszów
		</td>
		<td>
			Daniel
			Marczydło
			(III
			G, 15 l)
		</td>

		<td>
			Strona WWW
		</td>
		<td>
			Prąd elektryczny
		</td>
	</tr>
	<tr>
		<td>

			73.
		</td>
		<td>
			III
			LO w Rzeszowie
			ul.
			Szopena 11
			Rzeszów
			35-055
		</td>
		<td>
			Alina
			Biega
			(I
			LO, 16 l)
			Adrian
			Kraska
			(I
			LO, 16 l)
		</td>
		<td>
			Strona
			WWW
		</td>

		<td>
			Zjawiska optyczne
		</td>
	</tr>
	<tr>
		<td>
			75.
		</td>
		<td>

			Zespół
			Szkół Nr 2 w Rzeszowie
			Ul.
			Rejtana 3
			35-326
			Rzeszów
		</td>
		<td>
			Mirosław
			Rykała
			(II
			LO, 18 l)
			Grzegorz
			Rak
			(II
			LO, 18 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Strona WWW o Marii Skłodowskiej-Curie

		</td>
	</tr>
	<tr>
		<td>
			76.
		</td>
		<td>
			Zespół
			Szkół Mechanicznych im. Gen. Władysława Andersa w Rzeszowie
			Ul.
			Hetmańska 45a
		</td>
		<td>

			Joanna
			Piela
			(II,
			18 l)
			Łukasz
			Adamczyk
			(II,
			18 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Fizyka z Astronomią
		</td>
	</tr>

	<tr>
		<td>
			77.
		</td>
		<td>
			Zespół
			Szkół Technicznych im. E. Kwiatkowskiego
			Ul.
			Matuszczaka 7
			35-084
			Rzeszów
		</td>
		<td>
			Paweł
			Szela
			(I,
			16 l)
			Łukasz
			Świrk
			(I,
			16 l)
		</td>

		<td>
			Prezentacja
		</td>
		<td>
			Historia
			i budowa Wszechświata
		</td>
	</tr>
	<tr>
		<td>
			79.
		</td>

		<td>
			I
			LO im. Komisji Edukacji Narodowej
			Ul.
			Zagrody 1
			38-500
			Sanok
		</td>
		<td>
			Tomasz
			Wolański
			(II
			LO, 17 l)
			Tomasz
			Sośnicki
			(II
			LO, 17 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>

			Energia jądrowa i jej wykorzystanie
		</td>
	</tr>
	<tr>
		<td>
			80.
		</td>
		<td>
			Zespół
			Szkół Nr 3 w Sanoku
			Ul.
			Stróżowska 16
			38-500
			Sanok
		</td>

		<td>
			Idec
			Tomasz
			(II
			T, 17 l)
			Kot
			Krystian
			(II
			T, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Fale
			elektromagnetyczne &ndash; zastosowanie w moim miejscu
			zamieszkania
		</td>

	</tr>
	<tr>
		<td>
			81.
		</td>
		<td>
			Zespół
			Szkół Nr 3 w Sanoku
			Ul.
			Stróżowska 16
			38-500
			Sanok
		</td>
		<td>
			Hunia
			Mariusz
			(II
			T, 17 l)
			Kabala
			Piotr
			(II
			T, 17 l)
		</td>

		<td>
			Strona
			WWW
		</td>
		<td>
			Zjawisko fotoelektryczne
		</td>
	</tr>
	<tr>
		<td>

			83.
		</td>
		<td>
			Zespół
			Szkół Licealnych im. Z. Herberta
			Ul.
			Bohaterów Warszawy 3
			69-100
			Słubice
			<a href="mailto:loslubice@interia.pl">loslubice@interia.pl</a>
		</td>
		<td>
			Jaworski
			Michał
			(I
			, 17 l)
			Paczkowski
			Piotr
			(I
			, 17 l)
		</td>
		<td>

			Prezentacja
		</td>
		<td>
			Obwód
			LC
		</td>
	</tr>
	<tr>
		<td>
			84.
		</td>

		<td>
			Zespół
			Szkół Licealnych im. Z. Herberta
			Ul.
			Bohaterów Warszawy 3
			69-100
			Słubice
			<a href="mailto:loslubice@interia.pl">loslubice@interia.pl</a>
		</td>
		<td>
			Gembarowski
			Łukasz
			(I
			, 17 l)
			Andrzejewski
			Przemysław
			(I
			, 17 l)
		</td>
		<td>
			Program
			komp.
		</td>

		<td>
			Układ
			słoneczny
		</td>
	</tr>
	<tr>
		<td>
			85.
		</td>
		<td>
			Zespół
			Szkół Nr 3 im. Króla jana III Sobieskiego w Stalowej Woli
			Ul.
			Polna 15
			37-464
			Stalowa Wola
			t/f
			0-15 8440412
		</td>

		<td>
			Dawid
			Sobiło
			(II
			T Hand, 18 l)
			Łukasz
			Sobiło
			(II
			T Hand, 18 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Księżyc
		</td>

	</tr>
	<tr>
		<td>
			86.
		</td>
		<td>
			Zespół
			Szkół Zawodowych nr 2
			Ul.
			1-go Maja 4
			27-200
			Starachowice
		</td>
		<td>
			Piotr
			Jurkiewicz
			(III
			T, 18 l)
			Wojciech
			Niewczasa
			(III
			T, 18 l)
		</td>

		<td>
			Prezentacja
		</td>
		<td>
			Elektryczność
			&ndash; pionierzy elektryczności
		</td>
	</tr>
	<tr>
		<td>

			88.
		</td>
		<td>
			XIII
			LO
			Ul.
			Unisławy 26
			71-413
			Szczecin
		</td>
		<td>
			Piotr
			Andrzejewski
			(II
			LO)
			Michał
			Stefanowic
			(II
			LO)
		</td>
		<td>
			Strona
			WWW
		</td>

		<td>
			Gauss Carl Friedrich
		</td>
	</tr>
	<tr>
		<td>
			90.
		</td>
		<td>

			VI
			LO im. Stefana Czarnickiego w Szczecinie
			70-382
			Szczecin
			ul.
			Jagiellońska 41
		</td>
		<td>
			Karol
			Szczyciński
			(II
			LO , 17 l)
			Robert
			Mańko
			(II
			LO, 17 l)
		</td>
		<td>
			Strona WWW
		</td>
		<td>

			Kometa
			Halley&rsquo;a
		</td>
	</tr>
	<tr>
		<td>
			91.
		</td>
		<td>
			Zespół
			Szkół Ogólnokształcących Mistrzostwa Sportowego w Szczecinie
			Gimnazjum
			19
			Ul.
			Mazurska 40 
			
		</td>

		<td>
			Sebastian
			Desz
			(III
			G, 15 l)
			Karol
			Wal
			(III
			G, 15 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Fizyka
		</td>

	</tr>
	<tr>
		<td>
			92.
		</td>
		<td>
			Zespół
			Szkół Ogólnokształcących Mistrzostwa Sportowego w Szczecinie
			Gimnazjum
			19
			Ul.
			Mazurska 40 
			
		</td>
		<td>
			Szymon
			Myśliński
			(III
			G, 15 l)
			Sebastian
			Ortel
			(III
			G, 15 l)
		</td>

		<td>
			Prezentacja
		</td>
		<td>
			Fizyka
			i astronomia
		</td>
	</tr>
	<tr>
		<td>
			93.
		</td>

		<td>
			XIII
			LO
			Ul.
			Unisławy 26
			71-413
			Szczecin
		</td>
		<td>
			Leszek
			Karcz
			(II
			LO)
			<a href="mailto:mjm01@o2.pl">mjm01@o2.pl</a>
		</td>
		<td>
			Prezentacja
		</td>

		<td>
			MJM_INFIZ
		</td>
	</tr>
	<tr>
		<td>
			94.
		</td>
		<td>
			I
			LO im .Ks. Elżbiety w Szczecinku
			Ul.
			Ks. Elżbiety 1
			<a href="mailto:loela@poczta.onet.pl">loela@poczta.onet.pl</a>

		</td>
		<td>
			Paweł
			Galerczyk
			Grzegorz
			Kubicki
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Przygoda
			z piłką
		</td>

	</tr>
	<tr>
		<td>
			95.
		</td>
		<td>
			I
			LO im .Ks. Elżbiety w Szczecinku
			Ul.
			Ks. Elżbiety 1
			<a href="mailto:loela@poczta.onet.pl">loela@poczta.onet.pl</a>
		</td>

		<td>
			Paweł
			Galerczyk
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Fizyka
		</td>

	</tr>
	<tr>
		<td>
			96.
		</td>
		<td>
			ZST
			im. J. Mościckiego
			Ul.
			Kwiatkowskiego 17
			33-101
			Tarnów &ndash; Mościce
		</td>
		<td>

			Przemysław
			Kozaczka
			(II
			T, 17 l)
			Waldemar
			Juwa
			(II
			T, 17 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>
			Drgania
		</td>
	</tr>
	<tr>

		<td>
			97.
		</td>
		<td>
			ZST
			im. J. Mościckiego
			Ul.
			Kwiatkowskiego 17
			33-101
			Tarnów &ndash; Mościce
		</td>
		<td>
			Piotr
			Koza
			(II
			LO, 17 l)
			Wojciech
			Wałaszek
			(II
			LO, 17 l)
		</td>

		<td>
			Strona
			WWW
		</td>
		<td>
			Strona WWW o Marianie Smoluchowskim
		</td>
	</tr>
	<tr>
		<td>

			98.
		</td>
		<td>
			Powiatowy
			Zespół Szkół nr 1
			55-100
			Trzebnica
			ul.
			Wojska Polskiego 17
			<a href="mailto:pzs1@poczta.onet.pl">pzs1@poczta.onet.pl</a>
		</td>
		<td>
			Paweł
			Bogner
			(II
			G, 15 l)
			Michał
			Kolad
			(II
			G, 15 l)
		</td>
		<td>

			Strona
			WWW
		</td>
		<td>
			Einstein
		</td>
	</tr>
	<tr>
		<td>
			100.
		</td>

		<td>
			Liceum
			Nr 12 Fundacji Szkolnej
			Ul.
			Obrzeźna 12 A
			02-691
			Warszawa
		</td>
		<td>
			Wiktoria
			Mrozek
			(I
			LO, 16 l)
			Krzysztof
			Drzewiecki
			(I
			LO, 16 l)
		</td>
		<td>
			Prezentacja
		</td>
		<td>

			Zjawiska
			fizyczne w atmosferze
		</td>
	</tr>
	<tr>
		<td>
			101.
		</td>
		<td>
			XIV
			LO im. St. Staszica
			Ul.
			Nowowiejska 37a
			02-010
			Warszawska
		</td>

		<td>
			Krzysztof
			Krogulski
			(I
			LO, 16 l)
			Maciej
			Bulwacki
			(I
			LO, 16 l)
		</td>
		<td>
			Program
			komp.
		</td>
		<td>
			Doświadczenie
			Rutherforda
		</td>
	</tr>

	<tr>
		<td>
			102.
		</td>
		<td>
			LO
			im. M. Kopernika
			Ul.
			Kopernika 2a
			98-400
			Wieruszków
		</td>
		<td>
			Jan
			Nowak
			(I
			LO, 16 l)
		</td>

		<td>
			Prezentacja
			+
			strona WWW
		</td>
		<td>
			Ekspansja wszechświata
		</td>
	</tr>
	<tr>
		<td>

			103.
		</td>
		<td>
			LO
			im. M. Kopernika
			Ul.
			Kopernika 2a
			98-400
			Wieruszków
		</td>
		<td>
			Krzysztof
			Ratajczyk
			(II
			LO, 17 l)
			Dawid
			Sikora
			(II
			LO, 17 l)
		</td>
		<td>
			Strona
			WWW
		</td>

		<td>
			Burze
		</td>
	</tr>
	<tr>
		<td>
			104.
		</td>
		<td>

			Zespół
			Oświatowy Publiczne Gimnazjum w Radomyśli
			08-112
			Wiśniew
			Radomyśl
			46
			<a href="mailto:zoradomysl@interia.pl">zoradomysl@interia.pl</a>
		</td>
		<td>
			Dorota
			Jastrzębska
		</td>
		<td>
			Prezentacja
		</td>
		<td>

			Ktokolwiek
			widział, ktokolwiek wie...
			(o
			A. Einsteinie)
		</td>
	</tr>
	<tr>
		<td>
			106.
		</td>
		<td>
			Zespół
			Szkół Nr. 1 im. St. Saszica
			Ul.
			Oporowska 7
			99-300
			Kutno 
			
		</td>

		<td>
			Piotr
			Czajka
			(II
			LO, 17 l)
			Krzysztof
			Sobieszek
			(II
			LO, 17 l)
		</td>
		<td>
			Strona
			WWW
		</td>
		<td>
			Magnetyzm
		</td>
	</tr>
	</table>

<?php
	include("../footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

