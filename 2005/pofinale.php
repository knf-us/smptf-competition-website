<?php
	include("../header.php");
	p_header("Wyniki edycji 2004/2005");
	include("../menu.php");
?>

<h1>Podsumowanie konkursu</h1>

<p>
<a href="http://www.us.edu.pl/uniwersytet/prezentacja_db.php?nazwa=projekt&amp;czas=5&amp;auto=0&amp;sa=1">Zdjęcia</a> zrobione przez Biuro Promocji i Karier UŚ.<br/>
<a href="http://knf.us.edu.pl/knf/konkurs.php">Zdjęcia</a> zrobione przez organizatorów.
</p>

<p><span class="date">22 czerwca 2005</span> rozstrzygnięto Ogólnopolski Konkurs na Projekt Multimedialny z Fizyki przeznaczony dla uczniów szkół ponadgimnazjalnych oraz gimnazjalnych, którego głównym organizatorem jest Sekcja Młodych Polskiego Towarzystwa Fizycznego. Konkurs odbył się w ramach Światowego Roku Fizyki, a jego finał miał miejsce w Instytucie Fizyki im. Augusta Chełkowskiego Uniwersytetu Śląskiego w Katowicach.</p>
<p>Zadaniem uczestników było opracowanie, możliwie najciekawszego i najbardziej rzetelnego, projektu multimedialnego o charakterze popularno-naukowym. Prace konkursowe miały ponadto zachować przystępność przekazu oraz zainteresować potencjalnych odbiorców omawianą tematyką. Ze względu na dużą dowolność formy projektu organizatorzy postanowili podzielić prace na trzy kategorie:</p>
<ul>
	<li>strona internetowa (strona o tematyce ogólnej, lub poświęcona wybranemu zagadnieniu lub osobie znanego fizyka itp.),</li>
	<li>prezentacja multimedialna (przedstawiająca w sposób ciekawy wybrane zagadnienie, zjawisko fizyczne, badania prowadzone w światowych instytutach itp.),</li>
	<li>program komputerowy (przedstawiający symulację wybranego zjawiska fizycznego itp.).</li>
</ul>
<p>Spośród imponującej liczby nadesłanych prac Jury wybrało te najlepsze a ich twórcy zostali zaproszeni na uroczysty finał. Tam pod czujnym okiem Jury drużyny prezentowały swoje projekty i odpowiadały na pytania. Następnie odbyła się najprzyjemniejsza część imprezy – ogłoszenie wyników, rozdanie nagród rzeczowych i okolicznościowych dyplomów.</p>
<p>Ze względu na powodzenie przedsięwzięcia oraz duże zainteresowanie uczestników postanowiliśmy kontynuować jego organizację i dlatego zapraszamy wszystkich chętnych do wzięcia udziału w drugiej edycji Ogólnopolskiego Konkursu na Projekt Multimedialny z Fizyki.</p>
<p>A oto laureaci w poszczególnych kategoriach</p>

<h2>Strony WWW</h2>
<ol class="results">
<li class="emph">Dawid i Łukasz Sobiło, Zespół Szkół Nr 3 im. Króla jana III Sobieskiego w Stalowej Woli (<a href="http://server.phys.us.edu.pl/~smptf/final/085/index.html">zwycięska strona</a>)</li>
<li>Tomasz Wolański i Tomasz Soźnicki,  I LO im. Komisji Edukacji Narodowej w Sanoku (<a href="http://server.phys.us.edu.pl/~smptf/final/079/index.htm">nagrodzona strona</a>)</li>
<li>Janusz Mikrut i Joanna Rejdych, II LO im. K. K. Baczyńskiego w Chrzanowie (<a href="http://server.phys.us.edu.pl/~smptf/final/010/index.html">nagrodzona strona</a>)</li>
<li>Kamil Szyndel i Jakub Kołaczkowski, XXIX LO im. Hm. Jana Bytnara "Rudego" w Łodzi (<a href="http://server.phys.us.edu.pl/~smptf/final/049/index.htm">nagrodzona strona</a>, <a href="http://server.phys.us.edu.pl/~smptf/final/049/Mpemba.avi">filmik zaprezentowany na finale</a>)</li>
<li>ex equo:<br/>
Piotr Majdak i Michał Polak, IV LO im. KEN w Bielsku-Białej (<a href="http://server.phys.us.edu.pl/~smptf/final/003/index.html">nagrodzona strona</a>)<br/>
Magdalena Broda i Michał Giża, Gimnazjum nr 4 w Mielcu (<a href="http://server.phys.us.edu.pl/~smptf/final/051/index.html">nagrodzona strona</a>)<br/>
</li>
<li>Paweł Galerczyk, I LO im .Ks. Elżbiety w Szczecinku (<a href="http://knf.us.edu.pl/smptf/2005/prace/095/index.php">nagrodzona strona</a>, <a href="http://server.phys.us.edu.pl/~smptf/final/095/Konkurs.ppt">prezentacja finałowa</a>)</li>
</ol>

<h2>Programy Komputerowe</h2>
<ol class="results">
<li class="emph">Sylwester Sokołowski, I LO im. Henryka Sienkiewicza w Koluszkach (<a href="http://server.phys.us.edu.pl/~smptf/final/032/fizyka.tar.bz2">zwycięski program - archiwum tar.bz2 (otwierać WinRARem)</a>)</li>
<li>Krzysztof Krogulski i Maciej Bulwacki,  XIV LO im. St. Staszica w Warszawie (<a href="http://server.phys.us.edu.pl/~smptf/final/101/rutherford.tar.bz2">nagrodzony program - archiwum tar.bz2 (otwierać WinRARem)</a>)</li>
<li>Michał Świtakowski, II LO w Końskich (<a href="http://server.phys.us.edu.pl/~smptf/final/034/setup.exe">nagrodzona praca</a>)</li>
<li>ex equo:<br/>
Paweł Marcak i Sławomir Sijka,  Zespół Szkół Ponadgimnazjalnych w Bystrzycy Kłodzkiej (<a href="http://server.phys.us.edu.pl/~smptf/final/007/projektant.exe">nagrodzona praca</a>)<br/>
Gembarowski Łukasz i Andrzejewski Przemysław, Zespół Szkół Licealnych im. Z. Herberta w Słubicach (<a href="http://server.phys.us.edu.pl/~smptf/final/084/system.tar.bz2">nagrodzony program - archiwum tar.bz2 (otwierać WinRARem)</a>)<br/>
</li>
</ol>

<h2>Prezentacje multimedialne</h2>
<ol class="results">
<li class="emph">Idec Tomasz i Kot Krystian, Zespół Szkół Nr 3 w Sanoku</li>
<li>Justyna Kosmala,  ZSO nr I im. Mikołaja Kopernika w Katowicach</li>
<li>Konrad Turczyński,  I LO im. T. Kościuszki w Busku-Zdroju</li>
<li> Marlena Jędrocka i Anna Orloł,  Zespół Szkół w Czarnej - Publiczne Gimnazjum</li>
<li>Wojciech Leja, Liceum Ogólnokształcące Wyższej Szkoły Informatyki i Zarządzania w Rzeszowie</li>
<li>Jaworski Michał i Paczkowski Piotr, Zespół Szkół Licealnych im. Z. Herberta w Słubicach</li>
<li>Grzegorz Primus i Krzysztof Łoboda, VIII LO w Katowicach</li>
</ol>

<?php
	include("../footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

