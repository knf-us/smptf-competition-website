<?php
	include("header.php");
	p_header("Strona główna");
	include("menu.php");
?>

<h1>Strona główna</h1>

<h2>Aktualne informacje</h2>

<ul>
<li>Na podstronie <a href="2006/final.php">Finał konkursu</a> można już znaleźć
wyniki tegorocznej edycji konkursu.</li>
<li>Na podstronie <a href="2006/final.php">Finał konkursu</a> zamieściliśmy
mapkę dojazdową dla finalistów.</li>
<li>Do ściągnięcia jest już <a href="2006/docs/final_plan.pdf">wstępny plan finału</a> (aktualizacja: 2006-06-06) .</li>
<li>Do wszystkich uczestników, którzy wraz ze zgłoszeniem przysłali zaadresowaną kopertę
zostanie dnia 29. maja 2006 r. rozesłana informacja na temat wyników I etapu konkursu
i ew. zakwalifikowania się do finału.</li>
<li>Na podstronie <a href="2006/finalowe.php">Prace finałowe</a> zamieściliśmy
listę prac zakwalifikowanych do finału oraz dodatkowe informacje dla finalistów.</li>
<li>Miło nam poinformować, że wszystkie nadesłane prace pomyślnie przeszły wstępną 
kwalifikację techniczną.</li>
<li>Na podstronie <a href="2006/nadeslane.php">Nadesłane prace</a> została opublikoana
lista prac nadesłanych do tegorocznej edycji konkursu.</li>
<li>Na podstronie <a href="2006/zgloszone.php">Zgłoszone prace</a> została opublikowana
lista prac zgłoszonych do tegorocznej edycji konkursu.</li>
<li>Z powodu ferii zimowych termin nadsyłania zgłoszeń do Konkursu został 
przedłużony do <span class="date">15 lutego 2006</span>.</li>
</ul>

<h2>O konkursie</h2>

<p><span class="emph">Ogólnopolski Konkurs na Projekt Multimedialny z Fizyki</span> jest kierowany do uczniów
szkół średnich z całego kraju. Jego celem jest rozwinięcie 
znajomości różnorodnych zagadnień z zakresu fizyki i informatyki, 
umiejętności pracy zespołowej oraz posługiwania się nowoczesną techniką informatyczną 
i multimedialną wsród uczestników, a także popularyzacja fizyki wsród całego społeczeństwa.</p>

<p>Pierwsza edycja <span class="emph">Ogólnopolskiego Konkursu na Projekt Multimedialny z Fizyki</span>
odbyła się w roku szkolnym 2004/2005 i została zorganizowana z okazji Światowego Roku Fizyki.
Konkurs spotkał się z dużym zainteresowaniem młodzieży i nauczycieli, więc postanowiliśmy
kontynuować jego organizację. Wszystkich chętnych zapraszamy do wzięcia udziału w <span class="emph">drugiej 
edycji!</span></p>

<p>Wszystkie potrzebne informacje oraz dokumenty znajdują się w odpowiednich działach na naszej
STRONIE Internetowej. W razie jakichkolwiek pytań lub wątpliwości można kontaktować się z
<a href="organizatorzy.php">organizatorami</a>, którzy chętnie odpowiedzą na Wasze pytania.</p>

<?php
	include("footer.php");
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>

