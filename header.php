<?php

include("settings.php");
		
function p_header($title)
{
	global $root_uri;
		
	echo <<<HTML
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>$title</title>
	<link rel="stylesheet" href="$root_uri/css/main.css" type="text/css" />
</head>
<body>

<div id="container">
HTML;

	/* Detect buggy browsers such as MSIE and warn the users about
	 * what kind of software they're using. */
	if (preg_match("/MSIE/", $_SERVER["HTTP_USER_AGENT"])) {
		echo <<<HTML

<div class="header">
	<table border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="helper logo_cell"><img src="$root_uri/gfx/logo_smptf.png" alt="Logo SMPTF-u" /></td>
		<td class="helper"><p>Ogólnopolski Konkurs na Projekt <br />Multimedialny z Fizyki</p></td>
		<td class="helper logo_cell"><img src="$root_uri/gfx/logo_konkurs.png" alt="Logo konkursu" /></td>
	</tr>
	</table>
</div>

<div class="warning">
Używasz przeglądarki, która nie obsługuje poprawnie standardów W3C! Ta strona może być wyświetlona nieprawidłowo.
</div>

HTML;
	} else {
	echo <<<HTML

<div id="header">
	<div class="columns">
		<div class="wrapper">
			<div class="helper logo_cell"><img src="$root_uri/gfx/logo_smptf.png" alt="Logo SMPTF-u" /></div>
			<div class="helper"><p>Ogólnopolski Konkurs na Projekt <br />Multimedialny z Fizyki</p></div>
			<div class="helper logo_cell"><img src="$root_uri/gfx/logo_konkurs.png" alt="Logo konkursu" /></div>
		</div>
	</div>
</div>

HTML;
	}
}
		
		
/* vim: set ts=4 encoding=utf-8 nowrap : */
?>
